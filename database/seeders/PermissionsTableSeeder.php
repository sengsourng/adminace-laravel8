<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Admin\Permission;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            [
                'id'    => 1,
                'group' => 'Profile',
                'title' => 'auth_profile_edit',
            ],
            [
                'id'    => 2,
                'group' => 'User Management',
                'title' => 'user_management_access',
            ],
            [
                'id'    => 3,
                'group' => 'Permission',
                'title' => 'permission_create',
            ],
            [
                'id'    => 4,
                'group' => 'Permission',
                'title' => 'permission_edit',
            ],
            [
                'id'    => 5,
                'group' => 'Permission',
                'title' => 'permission_show',
            ],
            [
                'id'    => 6,
                'group' => 'Permission',
                'title' => 'permission_delete',
            ],
            [
                'id'    => 7,
                'group' => 'Permission',
                'title' => 'permission_access',
            ],
            [
                'id'    => 8,
                'group' => 'Role',
                'title' => 'role_create',
            ],
            [
                'id'    => 9,
                'group' => 'Role',
                'title' => 'role_edit',
            ],
            [
                'id'    => 10,
                'group' => 'Role',
                'title' => 'role_show',
            ],
            [
                'id'    => 11,
                'group' => 'Role',
                'title' => 'role_delete',
            ],
            [
                'id'    => 12,
                'group' => 'Role',
                'title' => 'role_access',
            ],
            [
                'id'    => 13,
                'group' => 'User',
                'title' => 'user_create',
            ],
            [
                'id'    => 14,
                'group' => 'User',
                'title' => 'user_edit',
            ],
            [
                'id'    => 15,
                'group' => 'User',
                'title' => 'user_show',
            ],
            [
                'id'    => 16,
                'group' => 'User',
                'title' => 'user_delete',
            ],
            [
                'id'    => 17,
                'group' => 'User',
                'title' => 'user_access',
            ],
        ];
        Permission::insert($permissions);
    }
}
