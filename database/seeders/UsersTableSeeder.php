<?php

namespace Database\Seeders;

use App\Models\Admin\User;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'id'             => 1,
                'name'           => 'Admin',
                'email'          => 'admin@admin.com',
                'password'       => bcrypt('12345678'),
                'remember_token' => Str::random(15),
                'current_team_id' => 1,
                'province_id' => 1,
                'username' => 'admin',
            ],
        ];

        User::insert($users);
    }
}
