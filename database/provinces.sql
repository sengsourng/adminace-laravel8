-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Sep 02, 2021 at 09:43 AM
-- Server version: 5.7.31
-- PHP Version: 7.4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `admin_ace`
--

-- --------------------------------------------------------

--
-- Table structure for table `provinces`
--

DROP TABLE IF EXISTS `provinces`;
CREATE TABLE IF NOT EXISTS `provinces` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `khmer_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `provinces`
--

INSERT INTO `provinces` (`id`, `type`, `code`, `khmer_name`, `name`, `created_at`, `updated_at`) VALUES
(1, 'ខេត្ត​', '1', 'បន្ទាយមានជ័យ', 'Banteay Meanchey', NULL, NULL),
(2, 'ខេត្ត​', '2', 'បាត់ដំបង', 'Battambang', NULL, NULL),
(3, 'ខេត្ត​', '3', 'កំពង់ចាម', 'Kampong Cham', NULL, NULL),
(4, 'ខេត្ត​', '4', 'កំពង់ឆ្នាំង', 'Kampong Chhnang', NULL, NULL),
(5, 'ខេត្ត​', '5', 'កំពង់ស្ពឺ', 'Kampong Speu', NULL, NULL),
(6, 'ខេត្ត​', '6', 'កំពង់ធំ', 'Kampong Thom', NULL, NULL),
(7, 'ខេត្ត​', '7', 'កំពត', 'Kampot', NULL, NULL),
(8, 'ខេត្ត​', '8', 'កណ្ដាល', 'Kandal', NULL, NULL),
(9, 'ខេត្ត​', '9', 'កោះកុង', 'Koh Kong', NULL, NULL),
(10, 'ខេត្ត​', '10', 'ក្រចេះ', 'Kratie', NULL, NULL),
(11, 'ខេត្ត​', '11', 'មណ្ឌលគិរី', 'Mondul Kiri', NULL, NULL),
(12, 'ខេត្ត​', '12', 'រាជធានីភ្នំពេញ', 'Phnom Penh', NULL, NULL),
(13, 'ខេត្ត​', '13', 'ព្រះវិហារ', 'Preah Vihear', NULL, NULL),
(14, 'ខេត្ត​', '14', 'ព្រៃវែង	', 'Prey Veng', NULL, NULL),
(15, 'ខេត្ត​', '15', 'ពោធិ៍សាត់	', 'Pursat', NULL, NULL),
(16, 'ខេត្ត​', '16', 'រតនគិរី	', 'Ratanak Kiri', NULL, NULL),
(17, 'ខេត្ត​', '17', 'សៀមរាប	', 'Siemreap', NULL, NULL),
(18, 'ខេត្ត​', '18', 'ព្រះសីហនុ	', 'Preah Sihanouk', NULL, NULL),
(19, 'ខេត្ត​', '19', 'ស្ទឹងត្រែង	', 'Stung Treng', NULL, NULL),
(20, 'ខេត្ត​', '20', 'ស្វាយរៀង	', 'Svay Rieng', NULL, NULL),
(21, 'ខេត្ត​', '21', 'តាកែវ	', 'Takeo', NULL, NULL),
(22, 'ខេត្ត​', '22', 'ឧត្ដរមានជ័យ	', 'Oddar Meanchey', NULL, NULL),
(23, 'ខេត្ត​', '23', 'កែប	', 'Kep', NULL, NULL),
(24, 'ខេត្ត​', '24', 'ប៉ៃលិន	', 'Pailin', NULL, NULL),
(25, 'ខេត្ត​', '25', 'ត្បូងឃ្មុំ	', 'Tboung Khmum', NULL, NULL);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
