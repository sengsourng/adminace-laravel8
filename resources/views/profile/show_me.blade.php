@extends('layouts.ace_admin_layout')

@section('admin_content')

    <div class="row">
			<div class="col-lg-6">
				<div class="page-header">
					<h4 class="page-title text-primary-d2">
						<a href="{{ route('admin.users.index') }}">{{ trans('cruds.user.title') }}</a>
						<span class="page-info text-secondary-d2 text-80">
							<i class="fa fa-angle-double-right text-80"></i>
							{{trans('global.my_profile')}}
						</span>
					</h4>
				</div>
			</div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <!-- Profile Photo -->
           @if (Laravel\Jetstream\Jetstream::managesProfilePhotos())
               <!-- Current Profile Photo -->
               <div class="mt-2 text-center">
                   <img style="width: 200px; height: 200px;" src="{{ Auth::user()->profile_photo_url }}" alt="{{Auth::user()->name}}">
               </div>
           @endif

            <div class="form-group pt-4">
                <label class="form-label" for="name">{{ __('global.user_name') }} : <span class="text-success">{{Auth::user()->name}}</span></label>
            </div>
            <div class="form-group pt-4">
                <label class="form-label" for="email">Email : <span class="text-success">{{Auth::user()->email}}</span></label>
            </div>


       </div>

			<div class="col-lg-6">
				<div class="card">
						<div class="card-header">
								<div class="card-header-container">
										<h6 class="card-title">
												{{ __('global.my_profile') }}
										</h6>
								</div>
						</div>

						<div class="card-body">
								<div class="pt-3">
										@livewire('update-profile-information-form')

										<hr class="mt-6 border-b-1 border-blueGray-300">

										@livewire('update-password-form')
								</div>
						</div>

				</div>
			</div>

    </div>

@endsection
