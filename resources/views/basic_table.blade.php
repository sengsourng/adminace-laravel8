{{-- <div>
	<div class="row mt-3">
			<div class="col-12">
				<div class="card dcard">
					<div class="card-body px-1 px-md-3">
						<div class="d-flex flex-column flex-sm-row mb-3 px-2 px-sm-0 justify-content-center">
							<h1 class="text-primary">
								{{ trans('cruds.user.title_singular') }}
								{{ trans('global.list') }}
							</h1>
						</div>
						<div class="d-flex justify-content-between flex-column flex-sm-row mb-3 px-2 px-sm-0">
							<div class="text-nowrap align-self-center align-self-sm-start pl-3">
								<span class="d-inline-block text-grey-d2">
									Per page:
								</span>
								<select wire:model="perPage" class="ml-3 ace-select no-border angle-down brc-h-blue-m3 w-auto pr-45 text-secondary-d3">
										@foreach($paginationOptions as $value)
											<option value="{{ $value }}">{{ $value }}</option>
										@endforeach
								</select>
							</div>
							<div class="text-nowrap align-self-center align-self-sm-start pl-3">
								@can('user_delete')
									<button class="btn btn-danger ml-3 disabled:opacity-50 disabled:cursor-not-allowed" type="button" wire:click="confirm('deleteSelected')" wire:loading.attr="disabled" {{ $this->selectedCount ? '' : 'disabled' }}>
										{{ __('Delete Selected') }}
									</button>
								@endcan
							</div>
							<div class="text-nowrap align-self-center align-self-sm-start pl-3">
								@if(file_exists(app_path('Http/Livewire/ExcelExport.php')))
									<livewire:excel-export model="Permission" format="csv" />
									<livewire:excel-export model="Permission" format="xlsx" />
									<livewire:excel-export model="Permission" format="pdf" />
								@endif
							</div>
							<div class="pos-rel ml-sm-auto mr-sm-2 order-last order-sm-0">
								<i class="fa fa-search position-lc ml-25 text-primary-m1"></i>
								<input wire:model.debounce.300ms="search" type="text" class="form-control w-100 pl-45 radius-1 brc-primary-m4" placeholder="Search ..." />
							</div>
							<div class="mb-2 mb-sm-0">
								<a href="{{ route('admin.users.create') }}" class="btn btn-blue px-3 d-block w-100 text-95 radius-round border-2 brc-black-tp10">
									<i class="fa fa-plus mr-1"></i>
									{{ trans('global.add') }} <span class="d-sm-none d-md-inline">{{ trans('global.new') }}</span> {{ trans('cruds.user.title_singular') }}
								</a>
							</div>
						</div>

						<table id="simple-table" class="mb-0 table table-striped table-bordered brc-secondary-l3 text-dark-m2 radius-1 overflow-hidden">
							<thead class="text-dark-tp3 bgc-grey-l4 text-90 border-b-1 brc-transparent">
								<tr>
									<th class="text-center pr-0">
										<label class="py-0">
											<input type="checkbox" class="align-bottom mb-n1 border-2 text-dark-m3" />
										</label>
									</th>
									<th>
										{{ trans('cruds.user.fields.id') }}
										@include('components.table.sort', ['field' => 'id'])
									</th>
									<th>
										{{ trans('cruds.user.fields.title') }}
										@include('components.table.sort', ['field' => 'title'])
									</th>
									<th>
										{{ trans('cruds.user.fields.roles') }}
									</th>
									<th>{{ trans('global.action') }}</th>
								</tr>
							</thead>
							<tbody class="mt-1">
								@forelse($users as $user)
									<tr class="bgc-h-yellow-l4 d-style">
										<td class='text-center pr-0 pos-rel'>
											<div class="position-tl h-100 ml-n1px border-l-4 brc-orange-m1 v-hover">
												<!-- border shown on hover -->
											</div>
											<div class="position-tl h-100 ml-n1px border-l-4 brc-success-m1 v-active">
												<!-- border shown when row is selected -->
											</div>
											<label>
												<input type="checkbox" class="align-middle" />
											</label>
										</td>
										<td>
											{{ $user->id }}
										</td>
										<td class="text-600 text-orange-d2">
											{{ $user->title }}
										</td>
										<td>
											@foreach($user->roles as $key => $entry)
													<span class="badge badge-info">{{ $entry->title }}</span>
											@endforeach
										</td>
										<td>
											<!-- action buttons -->
											<div class='d-none d-lg-flex'>
												<a href="#" class="mx-2px btn radius-1 border-2 btn-xs btn-brc-tp btn-light-secondary btn-h-lighter-warning btn-a-lighter-warning">
													<i class="fa fa-eye mx-1"></i>
												</a>
												<a href="#" class="mx-2px btn radius-1 border-2 btn-xs btn-brc-tp btn-light-secondary btn-h-lighter-success btn-a-lighter-success">
													<i class="fa fa-pencil-alt"></i>
												</a>
												<a href="#" class="mx-2px btn radius-1 border-2 btn-xs btn-brc-tp btn-light-secondary btn-h-lighter-danger btn-a-lighter-danger">
													<i class="fa fa-trash-alt"></i>
												</a>
											</div>
											<!-- show a dropdown in mobile -->
											<div class='dropdown d-inline-block d-lg-none dd-backdrop dd-backdrop-none-lg'>
												<a href='#' class='btn btn-default btn-xs py-15 radius-round dropdown-toggle' data-toggle="dropdown">
													<i class="fa fa-cog"></i>
												</a>
												<div class="dropdown-menu dd-slide-up dd-slide-none-lg">
													<div class="dropdown-inner">
														<a href="#" class="dropdown-item">
															<i class="fa fa-eye text-blue mr-1 p-2 w-4"></i>
															{{ trans('global.view') }}
														</a>
														<a href="#" class="dropdown-item">
															<i class="fa fa-pencil-alt text-blue mr-1 p-2 w-4"></i>
															{{ trans('global.edit') }}
														</a>
														<a href="#" class="dropdown-item">
															<i class="fa fa-trash-alt text-danger-m1 mr-1 p-2 w-4"></i>
															{{ trans('global.delete') }}
														</a>
													</div>
												</div>
											</div>
										</td>
									</tr>
								@empty
									<tr>
										<td colspan="10">No entries found.</td>
									</tr>
								@endforelse
							</tbody>
						</table>

						<!-- table footer -->
						<div class="d-flex pl-4 pr-3 pt-35 border-t-1 brc-secondary-l3 flex-column flex-sm-row mt-n1px justify-content-center">
								<div class="pt-3">
									@if($this->selectedCount)
										<p class="text-sm leading-5">
											<span class="font-medium">
												{{ $this->selectedCount }}
											</span>
											{{ __('Entries selected') }}
										</p>
									@endif
									{{ $users->links() }}
								</div>
						</div>
					</div><!-- /.card-body -->
				</div><!-- /.card -->
			</div><!-- /.col -->
	</div><!-- /.row -->
</div>
 --}}
