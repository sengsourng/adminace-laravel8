<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Laravel') }}</title>


            <link rel="stylesheet" type="text/css" href="{{ asset('backend/ace') }}/node_modules/bootstrap/dist/css/bootstrap.css">

            <link rel="stylesheet" type="text/css" href="{{ asset('backend/ace') }}/node_modules/@fortawesome/fontawesome-free/css/fontawesome.css">
            <link rel="stylesheet" type="text/css" href="{{ asset('backend/ace') }}/node_modules/@fortawesome/fontawesome-free/css/regular.css">
            <link rel="stylesheet" type="text/css" href="{{ asset('backend/ace') }}/node_modules/@fortawesome/fontawesome-free/css/brands.css">
            <link rel="stylesheet" type="text/css" href="{{ asset('backend/ace') }}/node_modules/@fortawesome/fontawesome-free/css/solid.css">

            <link rel="stylesheet" type="text/css" href="{{ asset('backend/ace') }}/dist/css/ace-font.css">
            <link rel="stylesheet" type="text/css" href="{{ asset('backend/ace') }}/dist/css/ace.css">
            <link rel="icon" type="image/png" href="{{ asset('backend/ace') }}/assets/favicon.png" />
            <link rel="stylesheet" type="text/css" href="{{ asset('backend/ace') }}/views/pages/page-login/@page-style.css">

             <!-- Fonts -->
            <link href="https://fonts.googleapis.com/css2?family=Hanuman:wght@400;600;700&display=swap" rel="stylesheet">
            <style>
                body {
                    font-family: 'Hanuman', sans-serif;
                }
            </style>

            @livewireStyles
    </head>
    <body>
            <div class="body-container">
                    <div class="main-container bgc-white">
                        <div role="main" class="main-content">
                            {{$slot}}
                        </div>
                    </div>
            </div>







            <script src="{{ asset('backend/ace') }}/node_modules/jquery/dist/jquery.js"></script>
            <script src="{{ asset('backend/ace') }}/node_modules/popper.js/dist/umd/popper.js"></script>
            <script src="{{ asset('backend/ace') }}/node_modules/bootstrap/dist/js/bootstrap.js"></script>
            <script src="{{ asset('backend/ace') }}/dist/js/ace.js"></script>
            <script src="{{ asset('backend/ace') }}/app/browser/demo.js"></script>
            <script src="{{ asset('backend/ace') }}/views/pages/page-login/@page-script.js"></script>

        @stack('modals')
        @livewireScripts

    </body>
</html>
