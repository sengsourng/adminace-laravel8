<script src="{{ asset('backend/ace') }}/node_modules/jquery/dist/jquery.js"></script>
<script src="{{ asset('backend/ace') }}/node_modules/popper.js/dist/umd/popper.js"></script>
<script src="{{ asset('backend/ace') }}/node_modules/bootstrap/dist/js/bootstrap.js"></script>
<script src="{{ asset('backend/ace') }}/node_modules/select2/dist/js/select2.js"></script>
<script src="{{ asset('backend/ace') }}/dist/js/ace.js"></script>
<script src="{{ asset('backend/ace/script_translate.js')}}"></script>
@livewireScripts
@stack('scripts')



