<div id="sidebar" class="sidebar sidebar-fixed expandable sidebar-light">
    <div class="sidebar-inner">

      <div class="ace-scroll flex-grow-1" data-ace-scroll="{}">
        <ul class="nav has-active-border active-on-right">
          <li class="nav-item {{ (request()->is('dashboard')) ? 'active' : '' }}">
            <a href="{{ Route('dashboard') }}" class="nav-link">
              <span class="nav-text fadeable">
                <span><i class="nav-icon fa fa-tachometer-alt"></i> {{ trans('global.dashboard') }}</span>
              </span>
            </a>
            <b class="sub-arrow"></b>
          </li>
					@can('user_management_access')
                        <li class="nav-item {{request()->routeIs('admin.permissions.index') ? 'active open' : '' }}">
                            <a href="javascript:void(0);" class="nav-link dropdown-toggle collapsed">
                            <span class="nav-text fadeable">
                                <span><i class="far fa-address-card"></i> {{ trans('cruds.userManagement.title') }}</span>
                            </span>
                            <b class="caret fa fa-angle-left rt-n90"></b>
                            </a>
                            <div class="hideable submenu collapse
                            {{request()->routeIs('admin.permissions.index') ? 'show' : '' }}
                            {{request()->routeIs('admin.roles.index') ? 'show' : '' }}
                            {{request()->routeIs('admin.users.index') ? 'show' : '' }}
                            ">
                            <ul class="submenu-inner">
                                                @can('permission_access')
                                                    <li class="nav-item {{ (request()->is('admin/permissions')) ? 'active' : '' }}">
                                                        <a href="{{ route('admin.permissions.index') }}" class="nav-link">
                                                        <span class="nav-text">
                                                            <span><i class="fas fa-user-secret"></i> {{ trans('cruds.permission.title') }}</span>
                                                        </span>
                                                        </a>
                                                    </li>
                                                @endcan
                                                @can('role_access')
                                                    <li class="nav-item {{request()->routeIs('admin.roles.index') ? 'active' : '' }}">
                                                        <a href="{{ route('admin.roles.index') }}" class="nav-link">
                                                            <span class="nav-text">
                                                                <span><i class="fas fa-users-cog mr-1 text-105"></i> {{ trans('cruds.role.title') }}</span>
                                                            </span>
                                                        </a>
                                                    </li>
                                                @endcan
                                                @can('user_access')
                                                    <li class="nav-item {{request()->routeIs('admin.users.index') ? 'active' : '' }}">
                                                        <a href="{{ route('admin.users.index') }}" class="nav-link">
                                                            <span class="nav-text">
                                                                <span><i class="fas fa-users"></i> {{ trans('cruds.user.title') }}</span>
                                                            </span>
                                                        </a>
                                                    </li>
                                                @endcan
                            </ul>
                            </div>
                            <b class="sub-arrow"></b>
                        </li>
					@endcan


{{-- Teacher --}}
                    @can('teacher_access')
                        <li class="nav-item">
                            <a href="javascript:void(0);" class="nav-link dropdown-toggle collapsed">
                            <span class="nav-text fadeable">
                                <span><i class="far fa-address-card"></i> {{ trans('cruds.teacher.title') }}</span>
                            </span>
                            <b class="caret fa fa-angle-left rt-n90"></b>
                            </a>
                            <div class="hideable submenu collapse">
                            <ul class="submenu-inner">
                                                @can('teacher_access')
                                                    <li class="nav-item">
                                                        <a href="{{ route('admin.permissions.index') }}" class="nav-link">
                                                        <span class="nav-text">
                                                            <span><i class="fas fa-user-secret"></i> {{ trans('cruds.teacher.title_singular') }}</span>
                                                        </span>
                                                        </a>
                                                    </li>
                                                @endcan

                            </ul>
                            </div>
                            <b class="sub-arrow"></b>
                        </li>
					@endcan



                    <li class="nav-item">
                        <a href="#" class="nav-link dropdown-toggle collapsed">
                        <span class="nav-text fadeable">
                        <span><i class="nav-icon fa fa-edit"></i> Forms</span>
                        </span>
                        <b class="caret fa fa-angle-left rt-n90"></b>
                        </a>
                        <div class="hideable submenu collapse">
                        <ul class="submenu-inner">
                            <li class="nav-item">
                            <a href="html/form-basic.html" class="nav-link">
                                <span class="nav-text">
                                    <span>Basic Elements</span>
                                </span>
                            </a>
                            </li>
                            <li class="nav-item">
                            <a href="html/form-more.html" class="nav-link">
                                <span class="nav-text">
                                    <span>More Elements</span>
                                </span>
                            </a>
                            </li>
                            <li class="nav-item">
                            <a href="html/form-wizard.html" class="nav-link">
                                <span class="nav-text">
                                    <span>Wizard &amp; Validation</span>
                                </span>
                            </a>
                            </li>
                            <li class="nav-item">
                            <a href="html/form-upload.html" class="nav-link">
                                <span class="nav-text">
                                    <span>File Upload</span>
                                </span>
                            </a>
                            </li>
                            <li class="nav-item">
                            <a href="html/form-wysiwyg.html" class="nav-link">
                                <span class="nav-text">
                                    <span>Wysiwyg &amp; Markdown</span>
                                </span>
                            </a>
                            </li>
                        </ul>
                        </div>
                        <b class="sub-arrow"></b>
                    </li>
        </ul>
      </div><!-- /.sidebar scroll -->

      <div class="sidebar-section">
        <div class="sidebar-section-item fadeable-bottom">
          <div class="fadeinable">
            <!-- shows this when collapsed -->
            <div class="pos-rel">
                @if (Laravel\Jetstream\Jetstream::managesProfilePhotos())
                    <img alt="Alexa's Photo" src="{{ Auth::user()->profile_photo_url }}" width="42" class="px-1px radius-round mx-2 border-2 brc-default-m2" />
                    <span class="bgc-success radius-round border-2 brc-white p-1 position-tr mr-1 mt-2px"></span>
                @endif

            </div>
          </div>
          <div class="fadeable hideable w-100 bg-transparent shadow-none border-0">
            <!-- shows this when full-width -->
            <div id="sidebar-footer-bg" class="d-flex align-items-center bgc-white shadow-sm mx-2 mt-2px py-2 radius-t-1 border-x-1 border-t-2 brc-primary-m3">
              <div class="d-flex mr-auto py-1">
                <div class="pos-rel">
                    @if (Laravel\Jetstream\Jetstream::managesProfilePhotos())
                        <img alt="Alexa's Photo" src="{{ Auth::user()->profile_photo_url }}" width="42" class="px-1px radius-round mx-2 border-2 brc-default-m2" />
                        <span class="bgc-success radius-round border-2 brc-white p-1 position-tr mr-1 mt-2px"></span>
                    @endif
                </div>

                <div>

                  <span class="text-blue-d1 font-bolder">{{Auth::user()->name}}</span>
                  <div class="text-80 text-grey">
                    @foreach(Auth::user()->roles as $key => $entry)
                        {{ $entry->title }}
                    @endforeach
                  </div>
                </div>
              </div>

              <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"
              class="d-style btn btn-outline-orange btn-h-light-orange btn-a-light-orange border-0 p-2 mr-1" title="Logout">
                <i class="fa fa-sign-out-alt text-150 text-orange-d2 f-n-hover"></i>
              </a>
                 <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                @csrf
                </form>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>
