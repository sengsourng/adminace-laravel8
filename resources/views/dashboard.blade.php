<x-ace-layout>

    <div class="page-content container container-plus">
        <!-- page header and toolbox -->
        <div class="page-header pb-2">
          <h1 class="page-title text-primary-d2 text-150">
            ផ្ទាំង​គ្រប់គ្រង
            <small class="page-info text-secondary-d2 text-nowrap">
              <i class="fa fa-angle-double-right text-80"></i>
              ពត៌មានលំអិតពីការគ្រប់គ្រង
            </small>
          </h1>



        </div>

        <div class="row mt-3">
          <div class="col-xl-12">
            <div class="row px-3 px-lg-4">

              <div class="col-12">
                <div class="row h-100 mx-n425">

                  <div class="col-12 col-sm-4 p-0 pos-rel mt-3 mt-sm-0 pt-0 pt-sm-0 text-center">
                    <div class="ccard bgc-primary-d2 h-100 d-flex flex-column mx-2 px-2 py-3">
                      <div class="d-flex text-white text-center">
                        <div class="flex-grow-1 mb-1">
                          <div class="text-nowrap text-100 text-white-l2">
                            ការបញ្ជាទិញកំពុងរង់ចាំ!
                          </div>
                          <div class="text-white">
                            <span class="text-150 text-white-d1 mr-n1">$</span>
                                <span class="text-170 text-white-d4">
                                        198,450
                                </span>

                                <span class="text-white text-nowrap ml-n1">
                                        +5% <i class="fa fa-caret-up"></i>
                                </span>
                          </div>
                          <div class="ml-auto pr-1 align-self-center pos-rel text-125">
                            <i class="fa fa-shopping-cart text-white opacity-1 fa-2x mr-25"></i>
                          </div>

                        </div>
                      </div>
                      <button type='button' class='btn btn-outline-blue text-white radius-round btn-bold px-4 py-1 mt-3 mx-auto'>មើលរបាយការណ៍</button>
                    </div><!-- /.ccard -->
                  </div><!-- /.col -->

                  <div class="col-12  col-sm-4 p-0 pos-rel mt-3 mt-sm-0 pt-0 pt-sm-0 text-center">
                    <div class="ccard bgc-green-d1 h-100 d-flex flex-column mx-2 px-2 py-3">
                      <div class="d-flex text-center">
                        <div class="flex-grow-1 mb-1">
                          <div class="text-nowrap text-100 text-white">
                            ដំណើរការបញ្ជាទិញ!
                          </div>
                          <div class="text-white">
                            <span class="text-150 text-160  mr-n1">$</span>
                                <span class="text-170 text-160 ">
                                        198,450
                                </span>

                                <span class="text-white text-nowrap ml-n1">
                                        +5% <i class="fa fa-caret-up"></i>
                                </span>
                          </div>
                          <div class="ml-auto pr-1 align-self-center pos-rel text-125">
                            <i class="fa fa-truck text-white opacity-1 fa-2x mr-25"></i>
                          </div>

                        </div>
                      </div>
                      <button type='button' class='btn btn-outline-red text-white radius-round btn-bold px-4 py-1 mt-3 mx-auto'>មើលរបាយការណ៍</button>
                    </div><!-- /.ccard -->
                  </div><!-- /.col -->

                  <div class="col-12 col-sm-4 p-0 pos-rel mt-3 mt-sm-0 pt-0 pt-sm-0 text-center">
                    <div class="ccard h-100 bgc-purple-d2 d-flex flex-column mx-2 px-2 py-3">
                      <div class="d-flex text-center">
                        <div class="flex-grow-1 mb-1 text-white">
                          <div class="text-nowrap text-100 text-white-l2">
                            ការបញ្ជាទិញបានបញ្ចប់!
                          </div>
                          <div>
                            <span class="text-150 text-white-d1 mr-n1">$</span>
                                <span class="text-170 text-white-d4">
                                        198,450
                                </span>

                                <span class="text-white text-nowrap ml-n1">
                                        +5% <i class="fa fa-caret-up"></i>
                                </span>
                          </div>
                          <div class="ml-auto pr-1 align-self-center pos-rel text-125">
                            <i class="fas fa-check-circle text-white opacity-1 fa-2x mr-25"></i>
                          </div>

                        </div>
                      </div>
                      <button type='button' class='btn btn-outline-blue text-white radius-round btn-bold px-4 py-1 mt-3 mx-auto'>មើលរបាយការណ៍</button>
                    </div><!-- /.ccard -->
                  </div><!-- /.col -->


                </div><!-- /.row -->
              </div>

              <div class="col-12 mt-35">
                <div class="row h-100 mx-n425">
                  <div class="col-12 col-md-4 px-0 mb-2 mb-md-0">
                    <div class="ccard h-100 pt-2 pb-25 px-25 d-flex mx-2 overflow-hidden">
                      <!-- the colored circles on bottom right -->
                      <div class="position-br	mb-n5 mr-n5 radius-round bgc-purple-l3 opacity-3" style="width: 5.25rem; height: 5.25rem;"></div>
                      <div class="position-br	mb-n5 mr-n5 radius-round bgc-purple-l2 opacity-5" style="width: 4.75rem; height: 4.75rem;"></div>
                      <div class="position-br	mb-n5 mr-n5 radius-round bgc-purple-l1 opacity-5" style="width: 4.25rem; height: 4.25rem;"></div>


                      <div class="flex-grow-1 pl-25 pos-rel d-flex flex-column">
                        <div class="text-secondary-d4">
                          <span class="text-200">
                                164
                            </span>
                            <span class="text-md text-danger-m1 align-text-bottom text-nowrap">
                                    (-2% <i class="ml-2px fa fa-caret-down"></i>)
                            </span>
                        </div>

                        <div class="mt-auto text-nowrap text-secondary-d2 text-105 letter-spacing mt-n1">
                          ការបញ្ជាទិញ
                        </div>

                      </div>
                      <div class="ml-auto pr-1 align-self-center pos-rel text-125">
                        <i class="fa fa-shopping-cart text-purple opacity-1 fa-2x mr-25"></i>
                      </div>
                    </div><!-- /.ccard -->
                  </div><!-- /.col -->



                  <div class="col-12 col-md-4 px-0 mb-2 mb-md-0">
                    <div class="ccard h-100 pt-2 pb-25 px-25 d-flex mx-2 overflow-hidden">
                      <!-- the colored circles on bottom right -->
                      <div class="position-br	mb-n5 mr-n5 radius-round bgc-blue-l3 opacity-3" style="width: 5.25rem; height: 5.25rem;"></div>
                      <div class="position-br	mb-n5 mr-n5 radius-round bgc-blue-l2 opacity-5" style="width: 4.75rem; height: 4.75rem;"></div>
                      <div class="position-br	mb-n5 mr-n5 radius-round bgc-blue-l1 opacity-5" style="width: 4.25rem; height: 4.25rem;"></div>


                      <div class="flex-grow-1 pl-25 pos-rel d-flex flex-column">
                        <div class="text-secondary-d4">
                          <span class="text-200">
                                750
                            </span>

                          <span class="text-md text-success-m1 align-text-bottom text-nowrap">
                                    (+8% <i class="ml-2px fa fa-caret-up"></i>)
                                </span>


                        </div>

                        <div class="mt-auto text-nowrap text-secondary-d2 text-105 letter-spacing mt-n1">
                          អតិថិជនថ្មី
                        </div>
                      </div>


                      <div class="ml-auto pr-1 align-self-center pos-rel text-125">
                        <i class="fa fa-users text-blue opacity-1 fa-2x mr-25"></i>
                      </div>
                    </div><!-- /.ccard -->
                  </div><!-- /.col -->



                  <div class="col-12 col-md-4 px-0 mb-2 mb-md-0">
                    <div class="ccard h-100 pt-2 pb-25 px-25 d-flex mx-2 overflow-hidden">
                      <!-- the colored circles on bottom right -->
                      <div class="position-br	mb-n5 mr-n5 radius-round bgc-orange-l3 opacity-3" style="width: 5.25rem; height: 5.25rem;"></div>
                      <div class="position-br	mb-n5 mr-n5 radius-round bgc-orange-l2 opacity-5" style="width: 4.75rem; height: 4.75rem;"></div>
                      <div class="position-br	mb-n5 mr-n5 radius-round bgc-orange-l1 opacity-5" style="width: 4.25rem; height: 4.25rem;"></div>


                      <div class="flex-grow-1 pl-25 pos-rel d-flex flex-column">
                        <div class="text-secondary-d4">
                          <span class="text-200">
                                16
                            </span>



                        </div>

                        <div class="mt-auto text-nowrap text-secondary-d2 text-105 letter-spacing mt-n1">
                            សរុបផលិតផល
                        </div>
                      </div>


                      <div class="ml-auto pr-1 align-self-center pos-rel text-125">
                        <i class="fa fa-shopping-bag text-orange opacity-1 fa-2x mr-25"></i>
                      </div>
                    </div><!-- /.ccard -->
                  </div><!-- /.col -->


                </div>
              </div>



              <div class="col-12 mt-35">
                <div class="row h-100 mx-n425">
                  <div class="col-12 col-md-4 px-0 mb-2 mb-md-0">
                    <div class="ccard h-100 pt-2 pb-25 px-25 d-flex mx-2 overflow-hidden">
                      <!-- the colored circles on bottom right -->
                      <div class="position-br	mb-n5 mr-n5 radius-round bgc-purple-l3 opacity-3" style="width: 5.25rem; height: 5.25rem;"></div>
                      <div class="position-br	mb-n5 mr-n5 radius-round bgc-purple-l2 opacity-5" style="width: 4.75rem; height: 4.75rem;"></div>
                      <div class="position-br	mb-n5 mr-n5 radius-round bgc-purple-l1 opacity-5" style="width: 4.25rem; height: 4.25rem;"></div>


                      <div class="flex-grow-1 pl-25 pos-rel d-flex flex-column">
                        <div class="text-secondary-d4">
                          <span class="text-200">
                                164
                            </span>
                            <span class="text-md text-danger-m1 align-text-bottom text-nowrap">
                                    (-2% <i class="ml-2px fa fa-caret-down"></i>)
                            </span>
                        </div>

                        <div class="mt-auto text-nowrap text-secondary-d2 text-105 letter-spacing mt-n1">
                          ការបញ្ជាទិញ
                        </div>

                      </div>
                      <div class="ml-auto pr-1 align-self-center pos-rel text-125">
                        <i class="fa fa-shopping-cart text-purple opacity-1 fa-2x mr-25"></i>
                      </div>
                    </div><!-- /.ccard -->
                  </div><!-- /.col -->



                  <div class="col-12 col-md-4 px-0 mb-2 mb-md-0">
                    <div class="ccard h-100 pt-2 pb-25 px-25 d-flex mx-2 overflow-hidden">
                      <!-- the colored circles on bottom right -->
                      <div class="position-br	mb-n5 mr-n5 radius-round bgc-blue-l3 opacity-3" style="width: 5.25rem; height: 5.25rem;"></div>
                      <div class="position-br	mb-n5 mr-n5 radius-round bgc-blue-l2 opacity-5" style="width: 4.75rem; height: 4.75rem;"></div>
                      <div class="position-br	mb-n5 mr-n5 radius-round bgc-blue-l1 opacity-5" style="width: 4.25rem; height: 4.25rem;"></div>


                      <div class="flex-grow-1 pl-25 pos-rel d-flex flex-column">
                        <div class="text-secondary-d4">
                          <span class="text-200">
                                750
                            </span>

                          <span class="text-md text-success-m1 align-text-bottom text-nowrap">
                                    (+8% <i class="ml-2px fa fa-caret-up"></i>)
                                </span>


                        </div>

                        <div class="mt-auto text-nowrap text-secondary-d2 text-105 letter-spacing mt-n1">
                          អតិថិជនថ្មី
                        </div>
                      </div>


                      <div class="ml-auto pr-1 align-self-center pos-rel text-125">
                        <i class="fa fa-users text-blue opacity-1 fa-2x mr-25"></i>
                      </div>
                    </div><!-- /.ccard -->
                  </div><!-- /.col -->



                  <div class="col-12 col-md-4 px-0 mb-2 mb-md-0">
                    <div class="ccard h-100 pt-2 pb-25 px-25 d-flex mx-2 overflow-hidden">
                      <!-- the colored circles on bottom right -->
                      <div class="position-br	mb-n5 mr-n5 radius-round bgc-orange-l3 opacity-3" style="width: 5.25rem; height: 5.25rem;"></div>
                      <div class="position-br	mb-n5 mr-n5 radius-round bgc-orange-l2 opacity-5" style="width: 4.75rem; height: 4.75rem;"></div>
                      <div class="position-br	mb-n5 mr-n5 radius-round bgc-orange-l1 opacity-5" style="width: 4.25rem; height: 4.25rem;"></div>


                      <div class="flex-grow-1 pl-25 pos-rel d-flex flex-column">
                        <div class="text-secondary-d4">
                          <span class="text-200">
                                16
                            </span>



                        </div>

                        <div class="mt-auto text-nowrap text-secondary-d2 text-105 letter-spacing mt-n1">
                            សរុបផលិតផល
                        </div>
                      </div>


                      <div class="ml-auto pr-1 align-self-center pos-rel text-125">
                        <i class="fa fa-shopping-bag text-orange opacity-1 fa-2x mr-25"></i>
                      </div>
                    </div><!-- /.ccard -->
                  </div><!-- /.col -->


                </div>
              </div>


            </div><!-- /.row -->
          </div>


        </div>





        <div class="row mt-5 pt-lg-3">
          <div class="col-12 col-lg-6">
            <div class="card ccard radius-t-0 h-100">
              <div class="position-tl w-102 border-t-3 brc-primary-tp3 ml-n1px mt-n1px"></div><!-- the blue line on top -->

              <div class="card-header pb-3 brc-secondary-l3">
                <h5 class="card-title mb-2 mb-md-0 text-dark-m3">
                  Transfers
                </h5>

                <div class="card-toolbar no-border pl-0 pl-md-2">
                  <a href="#" class="btn btn-sm btn-lighter-grey btn-bgc-white btn-h-light-orange btn-a-light-orange text-600 px-2 py-1 radius-round">
                    <i class="fa fa-arrow-down text-90 mx-1px"></i>
                  </a>
                </div>
              </div>

              <div class="card-body pt-2 pb-1">

                <div role="button" class="d-flex flex-wrap align-items-center my-2 bgc-secondary-l4 bgc-h-secondary-l3 radius-1 p-25 d-style">
                  <span class="mr-25 w-4 h-4 overflow-hidden text-center border-1 brc-secondary-m2 radius-round shadow-sm d-zoom-2">
                    <img alt="Alexa's avatar" src="{{asset('backend/ace')}}/assets/image/avatar/avatar3.jpg" class="h-4 w-4" />
                </span>

                  <span class="text-default-d3 text-90 text-600">
                    Alexa
                </span>

                  <span class="ml-auto text-dark-l2 text-nowrap">
                    1,250
                    <span class="text-80">
                        USD
                    </span>
                  </span>

                  <span class="ml-2">
                        <i class="fa fa-arrow-up text-green-m1 text-95"></i>
                </span>
                </div>
                <div role="button" class="d-flex flex-wrap align-items-center my-2 bgc-secondary-l4 bgc-h-secondary-l3 radius-1 p-25 d-style">
                  <span class="mr-25 w-4 h-4 overflow-hidden text-center border-1 brc-secondary-m2 radius-round shadow-sm d-zoom-2">
                    <img alt="Derek's avatar" src="{{asset('backend/ace')}}/assets/image/avatar/avatar2.png" class="h-4 w-4" />
                </span>

                  <span class="text-default-d3 text-90 text-600">
                    Derek
                </span>

                  <span class="ml-auto text-dark-l2 text-nowrap">
                    350
                    <span class="text-80">
                        EUR
                    </span>
                  </span>

                  <span class="ml-2">
                        <i class="fa fa-arrow-up text-green-m1 text-95"></i>
                </span>
                </div>
                <div role="button" class="d-flex flex-wrap align-items-center my-2 bgc-secondary-l4 bgc-h-secondary-l3 radius-1 p-25 d-style">
                  <span class="mr-25 w-4 h-4 overflow-hidden text-center border-1 brc-secondary-m2 radius-round shadow-sm d-zoom-2">
                    <img alt="Antonio's avatar" src="{{asset('backend/ace')}}/assets/image/avatar/avatar1.jpg" class="h-4 w-4" />
                </span>

                  <span class="text-default-d3 text-90 text-600">
                    Antonio
                </span>

                  <span class="ml-auto text-dark-l2 text-nowrap">
                    120
                    <span class="text-80">
                        CAD
                    </span>
                  </span>

                  <span class="ml-2">
                        <i class="fa fa-arrow-down text-danger-m1 text-95"></i>
                </span>
                </div>
                <div role="button" class="d-flex flex-wrap align-items-center my-2 bgc-secondary-l4 bgc-h-secondary-l3 radius-1 p-25 d-style">
                  <span class="mr-25 w-4 h-4 overflow-hidden text-center border-1 brc-secondary-m2 radius-round shadow-sm d-zoom-2">
                    <img alt="Gabriel's avatar" src="{{asset('backend/ace')}}/assets/image/avatar/avatar2.jpg" class="h-4 w-4" />
                </span>

                  <span class="text-default-d3 text-90 text-600">
                    Gabriel
                </span>

                  <span class="ml-auto text-dark-l2 text-nowrap">
                    620
                    <span class="text-80">
                        GBP
                    </span>
                  </span>

                  <span class="ml-2">
                        <i class="fa fa-arrow-up text-green-m1 text-95"></i>
                </span>
                </div>
                <div role="button" class="d-flex flex-wrap align-items-center my-2 bgc-secondary-l4 bgc-h-secondary-l3 radius-1 p-25 d-style">
                  <span class="mr-25 w-4 h-4 overflow-hidden text-center border-1 brc-secondary-m2 radius-round shadow-sm d-zoom-2">
                    <img alt="David's avatar" src="{{asset('backend/ace')}}/assets/image/avatar/avatar5.png" class="h-4 w-4" />
                </span>

                  <span class="text-default-d3 text-90 text-600">
                    David
                </span>

                  <span class="ml-auto text-dark-l2 text-nowrap">
                    330
                    <span class="text-80">
                        AUD
                    </span>
                  </span>

                  <span class="ml-2">
                        <i class="fa fa-arrow-down text-danger-m1 text-95"></i>
                </span>
                </div>
                <div role="button" class="d-flex flex-wrap align-items-center my-2 bgc-secondary-l4 bgc-h-secondary-l3 radius-1 p-25 d-style">
                  <span class="mr-25 w-4 h-4 overflow-hidden text-center border-1 brc-secondary-m2 radius-round shadow-sm d-zoom-2">
                    <img alt="Jason's avatar" src="{{asset('backend/ace')}}/assets/image/avatar/avatar6.jpg" class="h-4 w-4" />
                </span>

                  <span class="text-default-d3 text-90 text-600">
                    Jason
                </span>

                  <span class="ml-auto text-dark-l2 text-nowrap">
                    1,400
                    <span class="text-80">
                        AED
                    </span>
                  </span>

                  <span class="ml-2">
                        <i class="fa fa-arrow-down text-danger-m1 text-95"></i>
                </span>
                </div>
                <div role="button" class="d-flex flex-wrap align-items-center my-2 bgc-secondary-l4 bgc-h-secondary-l3 radius-1 p-25 d-style">
                  <span class="mr-25 w-4 h-4 overflow-hidden text-center border-1 brc-secondary-m2 radius-round shadow-sm d-zoom-2">
                    <img alt="Rebecca's avatar" src="{{asset('backend/ace')}}/assets/image/avatar/avatar7.jpg" class="h-4 w-4" />
                </span>

                  <span class="text-default-d3 text-90 text-600">
                    Rebecca
                </span>

                  <span class="ml-auto text-dark-l2 text-nowrap">
                    350
                    <span class="text-80">
                        USD
                    </span>
                  </span>

                  <span class="ml-2">
                        <i class="fa fa-arrow-up text-green-m1 text-95"></i>
                </span>
                </div>

              </div>
            </div>
          </div>


          <div class="col-12 col-lg-5 mt-4 mt-lg-0">
            <div id="conversations" class="card border-0 shadow-sm h-100">

              <div class="card-header bgc-primary-d1">
                <h6 class="card-title text-white font-normal">
                  <i class="far fa-comment-dots text-130 mr-1"></i>
                  <span class="text-110">
                    Conversation
                </span>
                  <span class="text-95 d-block d-sm-inline text-center">
                    (6 online)
                </span>
                </h6>

                <div class="card-toolbar align-self-center text-white text-90 no-border p-2px">
                  Jack is typing
                  <div class="typing-dots text-160 text-white mx-md-1">
                    <span class="typing-dot">.</span>
                    <span class="typing-dot">.</span>
                    <span class="typing-dot">.</span>
                  </div>
                </div>

                <div class="card-toolbar align-self-center">
                  <a href="#" data-action="reload" class="card-toolbar-btn text-white">
                    <i class="fas fa-sync-alt"></i>
                  </a>
                </div>
              </div>


              <div class="card-body border-x-1 brc-primary-l1 p-0">
                <div class="ace-scroll" data-ace-scroll='{"height": 380, "smooth":true}'>
                  <!-- if conversation item is on left -->
                  <div class="px-3 conversation mt-425">
                    <div class="d-flex align-items-start col px-0">
                      <div class="mr-3 mt-2px">
                        <div class="pos-rel">
                          <img alt="Max's avatar" src="{{asset('backend/ace')}}/assets/image/avatar/avatar1.jpg" class="radius-round w-4 h-4" />
                          <span class="position-tr bgc-success-d1 brc-white border-2 p-1 mt-n1 radius-round"></span>
                        </div>
                        <div class="text-600 text-90 ml-1">
                          <a href="#" class="font-bolder btn-text-dark btn-h-text-primary">
                            Max
                          </a>
                        </div>
                      </div>

                      <div class="col px-0">
                        <div class="d-flex mb-15 pos-rel">
                          <!-- the triangle -->
                          <span class="position-tl ml-n15 mt-15 w-3 h-3 bgc-grey-l3 rotate-n45"></span>

                          <div class="py-2 px-3 radius-1 bgc-grey-l3 text-dark-m1 pos-rel">
                            <div class="text-90" style="max-width: 480px;">
                              Raw denim you probably haven't heard of actively? So lorem ipsum indeed! <span class='fa-stack w-auto'> <i class='fas fa-circle text-dark fa-stack-1x text-100'></i> <i class='fas fa-smile-beam text-warning-m3 text-125 fa-stack-1x pos-rel'></i> </span>
                            </div>
                          </div>

                          <div class="text-80 text-grey-m2 ml-2 mt-2">
                            10:21
                          </div>
                        </div>
                        <div class="d-flex mb-15 pos-rel">

                          <div class="py-2 px-3 radius-1 bgc-grey-l3 text-dark-m1 pos-rel">
                            <div class="text-90" style="max-width: 480px;">
                              Tell you what, dolor sit amet forever!
                            </div>
                          </div>

                          <div class="text-80 text-grey-m2 ml-2 mt-2">
                            10:23
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>




                  <!-- if conversation item is on left -->
                  <div class="px-3 conversation text-right mt-425">
                    <div class="d-flex flex-row-reverse align-items-start col px-0">
                      <div class="ml-3 mt-2px">
                        <div class="pos-rel">
                          <img alt="Mia's avatar" src="{{asset('backend/ace')}}/assets/image/avatar/avatar7.jpg" class="radius-round w-4 h-4" />
                          <span class="position-tr bgc-orange-d1 brc-white border-2 p-1 mt-n1 radius-round"></span>
                        </div>
                        <div class="text-600 text-90 ml-1">
                          <a href="#" class="font-bolder btn-text-dark btn-h-text-primary">
                            Mia
                          </a>
                        </div>
                      </div>

                      <div class="col px-0">
                        <div class="d-flex flex-row-reverse mb-15 pos-rel">
                          <!-- the triangle -->
                          <span class="position-tr mr-n15 mt-15 w-3 h-3 bgc-primary-l3 rotate-n45"></span>

                          <div class="py-2 px-3 radius-1 bgc-primary-l3 text-dark-m1 pos-rel">
                            <div class="text-90" style="max-width: 480px;">
                              Consectetur adipiscing elit, quis nostrud exercitation.
                            </div>
                          </div>

                          <div class="text-80 text-grey-m2 mr-2 mt-2">
                            10:25
                          </div>
                        </div>
                        <div class="d-flex flex-row-reverse mb-15 pos-rel">

                          <div class="py-2 px-3 radius-1 bgc-primary-l3 text-dark-m1 pos-rel">
                            <div class="text-90" style="max-width: 480px;">
                              So count me in! <i class='fa fa-thumbs-up text-orange-m1 text-125 ml-1'></i>
                            </div>
                          </div>

                          <div class="text-80 text-grey-m2 mr-2 mt-2">
                            10:25
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- if conversation item is on left -->
                  <div class="px-3 conversation mt-425">
                    <div class="d-flex align-items-start col px-0">
                      <div class="mr-3 mt-2px">
                        <div class="pos-rel">
                          <img alt="Max's avatar" src="{{asset('backend/ace')}}/assets/image/avatar/avatar1.jpg" class="radius-round w-4 h-4" />
                          <span class="position-tr bgc-success-d1 brc-white border-2 p-1 mt-n1 radius-round"></span>
                        </div>
                        <div class="text-600 text-90 ml-1">
                          <a href="#" class="font-bolder btn-text-dark btn-h-text-primary">
                            Max
                          </a>
                        </div>
                      </div>

                      <div class="col px-0">
                        <div class="d-flex mb-15 pos-rel">
                          <!-- the triangle -->
                          <span class="position-tl ml-n15 mt-15 w-3 h-3 bgc-grey-l3 rotate-n45"></span>

                          <div class="py-2 px-3 radius-1 bgc-grey-l3 text-dark-m1 pos-rel">
                            <div class="text-90" style="max-width: 480px;">
                              Heard of them jean shorts Austin! Fusce dapibus, tellus ac cursus commodo, tortor mauris
                            </div>
                          </div>

                          <div class="text-80 text-grey-m2 ml-2 mt-2">
                            10:26
                          </div>
                        </div>
                        <div class="d-flex mb-15 pos-rel">

                          <div class="mt-2px pos-rel">
                            <div class='d-flex'>
                              <div class='border-1 radius-2 mx-1 brc-primary-l1 shadow-sm d-style overflow-hidden'><img alt='Heedphones' src='{{asset('backend/ace')}}/assets/image/product/wireless-headphone.jpg' width='64' class='d-zoom-2' /></div>
                              <div class='border-1 radius-2 mx-1 brc-primary-l1 shadow-sm d-style overflow-hidden'><img alt='Speakers' src='{{asset('backend/ace')}}/assets/image/product/smart-speaker.jpg' class='d-zoom-2' width='64' /></div>
                              <div class='border-1 radius-2 mx-1 brc-primary-l1 shadow-sm d-style overflow-hidden'><img alt='Shoes' src='{{asset('backend/ace')}}/assets/image/product/running-shoes.jpg' class='d-zoom-2' width='64' /></div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>




                  <!-- if conversation item is on left -->
                  <div class="px-3 conversation text-right mt-425">
                    <div class="d-flex flex-row-reverse align-items-start col px-0">
                      <div class="ml-3 mt-2px">
                        <div class="pos-rel">
                          <img alt="Mia's avatar" src="{{asset('backend/ace')}}/assets/image/avatar/avatar7.jpg" class="radius-round w-4 h-4" />
                          <span class="position-tr bgc-orange-d1 brc-white border-2 p-1 mt-n1 radius-round"></span>
                        </div>
                        <div class="text-600 text-90 ml-1">
                          <a href="#" class="font-bolder btn-text-dark btn-h-text-primary">
                            Mia
                          </a>
                        </div>
                      </div>

                      <div class="col px-0">
                        <div class="d-flex flex-row-reverse mb-15 pos-rel">
                          <!-- the triangle -->
                          <span class="position-tr mr-n15 mt-15 w-3 h-3 bgc-primary-l3 rotate-n45"></span>

                          <div class="py-2 px-3 radius-1 bgc-primary-l3 text-dark-m1 pos-rel">
                            <div class="text-90" style="max-width: 480px;">
                              Are you sure fusce dapibus tellus ac cursus commodo??? <span class='fa-stack w-auto'> <i class='fas fa-circle text-dark fa-stack-1x text-100'></i> <i class='fas fa-grin-alt text-warning-m3 text-125 fa-stack-1x pos-rel'></i> </span>
                            </div>
                          </div>

                          <div class="text-80 text-grey-m2 mr-2 mt-2">
                            10:49
                          </div>
                        </div>
                        <div class="d-flex flex-row-reverse mb-15 pos-rel">

                          <div class="py-2 px-3 radius-1 bgc-primary-l3 text-dark-m1 pos-rel">
                            <div class="text-90" style="max-width: 480px;">
                              Considering donec id elit non mi porta gravida at eget metus is undermined
                            </div>
                          </div>

                          <div class="text-80 text-grey-m2 mr-2 mt-2">
                            10:50
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>


              <!-- conversation footer -->
              <div class="card-footer px-25 pt-1px bgc-white border-1 border-t-0 brc-primary-l1 radius-b-2">
                <div class="mb-0 p-3 dcard brc-grey-l1">
                  <div class="input-group input-group-fade">
                    <input id="id-reply" type="text" class="form-control pl-2 border-0 radius-0 shadow-none" placeholder="Write your message ..." />

                    <!-- smiley dropdown -->
                    <div class="btn-group mx-2 dropup">
                      <button type="button" class="btn btn-xs px-1 border-0 btn-lighter-warning btn-bgc-tp dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="fa-stack">
                                <i class="far fa-smile text-dark-tp5 text-150 fa-stack-1x"></i>
                            </span>
                      </button>

                      <div class="dropdown-menu dropdown-menu-right p-1">
                        <div class="d-flex">
                          <a class="dropdown-item px-25" href="#">
                            <span class="fa-stack w-auto">
                                        <i class="fas fa-circle text-dark fa-stack-1x text-100"></i>
                                        <i class="fas fa-smile text-warning-m3 text-125 fa-stack-1x pos-rel"></i>
                                    </span>
                          </a>

                          <a class="dropdown-item px-25" href="#">
                            <span class="fa-stack w-auto">
                                        <i class="fas fa-circle text-dark fa-stack-1x text-100"></i>
                                        <i class="fas fa-smile-beam text-warning-m3 text-125 fa-stack-1x pos-rel"></i>
                                    </span>
                          </a>

                          <a class="dropdown-item px-25" href="#">
                            <span class="fa-stack w-auto">
                                        <i class="fas fa-circle text-dark fa-stack-1x text-100"></i>
                                        <i class="fas fa-smile-wink text-warning-m3 text-125 fa-stack-1x pos-rel"></i>
                                    </span>
                          </a>
                        </div>
                      </div>
                    </div>

                    <div class="input-group-append">
                      <button class="btn btn-info radius-3px" type="button">
                        Send
                        <i class="fa fa-angle-right ml-1"></i>
                      </button>
                    </div>
                  </div>
                </div>
              </div><!-- /.card-footer -->

            </div><!-- /.card -->
          </div>
        </div>
    </div>

    @push('modals')
        <!-- "Dashboard" page script to enable its demo functionality -->
        <script src="{{asset('backend/ace')}}/views/pages/dashboard/@page-script.js"></script>
    @endpush
</x-ace-layout>
