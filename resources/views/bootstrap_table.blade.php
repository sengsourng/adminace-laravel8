{{-- <div class="fixed-table-container" style="padding-bottom: 0px;"> --}}
{{-- <div class="fixed-table-body">
    <table class="table text-dark-m2 text-95 bgc-white ml-n1px table-bordered table-hover">
        <thead class="bgc-white text-white text-uppercase text-100">
            <tr class="bgc-info text-white brc-black-tp10">
                <th class="detail" style="width: 3%">
                        <div class="fht-cell"></div>
                </th>
                <th class="bs-checkbox" style="width: 5%">
                    <div class="th-inner">
                        <label>
                            <input name="btSelectAll" type="checkbox"><span></span>
                        </label>
                    </div>
                    <div class="fht-cell"></div>
                </th>
                <th style="width: 5%">
                        <div class="th-inner sortable both">
                            {{ trans('cruds.user.fields.id') }}
                            @include('components.table.sort', ['field' => 'id'])
                        </div>
                        <div class="fht-cell"></div>
                </th>
                <th style="width: 77%">
                        <div class="th-inner sortable both">
                            {{ trans('cruds.permission.title') }} {{ trans('cruds.permission.fields.title') }}
                            @include('components.table.sort', ['field' =>  'name'])
                        </div>
                        <div class="fht-cell"></div>
                </th>
                <th style="text-align: center; width: 10% ">
                        <div class="th-inner "><i class="fa fa-cog text-secondary-d1 text-130"></i></div>
                        <div class="fht-cell"></div>
                </th>
            </tr>
        </thead>
        <tbody>
            @forelse($permissions as $permission)
                <tr data-index="9" data-has-detail-view="true">
                        <td>
                                <a class="detail-icon" href="#">
                                <i class="fa fa-plus text-blue"></i>
                                </a>
                        </td>
                        <td class="bs-checkbox ">
                                <label>
                                        <input data-index="9" name="btSelectItem" type="checkbox">
                                        <span></span>
                                </label>
                        </td>
                        <td>{{ $permission->id }}</td>
                        <td>{{ $permission->title }}</td>
                        <td>
                                <div class="action-buttons">
                                        <a class="text-blue mx-1" href="#"><i class="fa fa-search-plus text-105"></i></a>
                                        <a class="text-success mx-1" href="#"><i class="fa fa-pencil-alt text-105"></i></a>
                                        <a class="text-danger-m1 mx-1" href="#"><i class="fa fa-trash-alt text-105"></i></a>
                                </div>
                        </td>
                </tr>
            @empty

            @endforelse
        </tbody>
    </table>
</div> --}}


{{-- <div class="table-responsive-md">
	<table class="table table-bordered border-0 table-striped-secondary text-dark-m1 mb-0">
		<thead>
			<tr class="bgc-info text-white brc-black-tp10">
																		<th class="bs-checkbox" style="width: 5%">
																				<div class="th-inner">
																						<label>
																								<input name="btSelectAll" type="checkbox"><span></span>
																						</label>
																				</div>
																				<div class="fht-cell"></div>
																		</th>
				<th>
																				<div class="th-inner sortable">
																						{{ trans('cruds.permission.fields.id') }}
																						@include('components.table.sort', ['field' => 'id'])
																				</div>
																		</th>
				<th>
																				<div class="th-inner sortable">
																						{{ trans('cruds.permission.fields.title') }}
																						@include('components.table.sort', ['field' => 'title'])
																				</div>
																		</th>
				<th>
																				<div class="th-inner "><i class="fa fa-cog text-secondary-d1 text-130"></i></div>
																		</th>
			</tr>
		</thead>

		<tbody>
			@forelse ($permissions as $permission)
					<tr>
							<td class="bs-checkbox ">
									<label>
											<input name="btSelectItem" type="checkbox">
									</label>
							</td>
							<td>{{ $permission->id }}</td>
							<td>{{ $permission->title }}</td>
							<td>
									<div class="action-buttons">
													<a class="text-blue mx-1" href="#"><i class="fa fa-search-plus text-105"></i></a>
													<a class="text-success mx-1" href="#"><i class="fa fa-pencil-alt text-105"></i></a>
													<a class="text-danger-m1 mx-1" href="#"><i class="fa fa-trash-alt text-105"></i></a>
									</div>
							</td>
					</tr>
			@empty

			@endforelse
		</tbody>
	</table>
</div> --}}
