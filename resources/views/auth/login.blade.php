{{-- <x-guest-layout>
    <x-jet-authentication-card>
        <x-slot name="logo">
            <x-jet-authentication-card-logo />
        </x-slot>

        <x-jet-validation-errors class="mb-4" />

        @if (session('status'))
            <div class="mb-4 font-medium text-sm text-green-600">
                {{ session('status') }}
            </div>
        @endif

        <form method="POST" action="{{ route('login') }}">
            @csrf

            <div>
                <x-jet-label for="email" value="{{ __('Email') }}" />
                <x-jet-input id="email" class="block mt-1 w-full" type="email" name="email" :value="old('email')" required autofocus />
            </div>

            <div class="mt-4">
                <x-jet-label for="password" value="{{ __('Password') }}" />
                <x-jet-input id="password" class="block mt-1 w-full" type="password" name="password" required autocomplete="current-password" />
            </div>

            <div class="block mt-4">
                <label for="remember_me" class="flex items-center">
                    <x-jet-checkbox id="remember_me" name="remember" />
                    <span class="ml-2 text-sm text-gray-600">{{ __('Remember me') }}</span>
                </label>
            </div>

            <div class="flex items-center justify-end mt-4">
                @if (Route::has('password.request'))
                    <a class="underline text-sm text-gray-600 hover:text-gray-900" href="{{ route('password.request') }}">
                        {{ __('Forgot your password?') }}
                    </a>
                @endif

                <x-jet-button class="ml-4">
                    {{ __('Log in') }}
                </x-jet-button>
            </div>
        </form>
    </x-jet-authentication-card>
</x-guest-layout> --}}

<x-login-layout>
	<div class="body-container">
		<div class="main-container container bgc-transparent">
			<div class="main-content minh-80 justify-content-center">
				<div class="p-2 p-md-5">
					<div class="row" id="row-1">
						<div class="col-12 col-xl-10 offset-xl-1 bgc-white shadow radius-1 overflow-hidden">
							<div class="row" id="row-2">
								<div id="id-col-intro" class="col-lg-5 d-none d-lg-flex border-r-1 brc-default-l3 px-0">
									<!-- the left side section is carousel in this demo, to show some example variations -->
									<div id="loginBgCarousel" class="carousel slide minw-100 h-100">
										<ol class="d-none carousel-indicators">
											<li data-target="#loginBgCarousel" data-slide-to="0" class="active"></li>
											<li data-target="#loginBgCarousel" data-slide-to="1"></li>
											<li data-target="#loginBgCarousel" data-slide-to="2"></li>
											<li data-target="#loginBgCarousel" data-slide-to="3"></li>
										</ol>

										<div class="carousel-inner minw-100 h-100">
											<div class="carousel-item active minw-100 h-100">
												<!-- default carousel section that you see when you open login page -->
												<div style="background-image: url({{asset('ace/assets/image/login-bg-1.svg')}});" class="px-3 bgc-blue-l4 d-flex flex-column align-items-center justify-content-center">
													<a class="mt-5 mb-2" href="{{route('home')}}">
														{{-- <i class="fa fa-leaf text-success-m2 fa-3x"></i> --}}
														<img height="120" src="{{asset('image/logo.png')}}" alt="">
													</a>

													<h2 class="text-primary-d1">
															ចូលប្រើ <span class="text-110 text-dark-l1">ហាងខ្មែរ</span>
															{{-- <img height="120" src="{{asset('image/logo_banner.png')}}" alt=""> --}}
															{{-- <img height="120" src="{{asset('image/logo-512.png')}}" alt=""> --}}
													</h2>

													<div class="mt-5 mx-4 text-dark-tp3">
														<span class="text-120">
															ចុះឈ្មោះ ជាមួយ​​ហាងយើងខ្ញុំ​,<br /> ដើម្បីទទួលបានទំនិញល្អៗ និង​ជឿជាក់!
														</span>
														<hr class="mb-1 brc-black-tp10" />
														<div>
														<a id="id-start-carousel" href="#" class="text-95 text-dark-l2 d-inline-block mt-3">
																<i class="far fa-image text-110 text-purple-m1 mr-1 w-2"></i>
																ប្តូររូបភាពខាងក្រោយ
														</a>
														<br />
														<a id="id-remove-carousel" href="#" class="text-md text-dark-l2 d-inline-block mt-3">
																<i class="far fa-trash-alt text-110 text-orange-d1 mr-1 w-2"></i>
																ដកផ្នែកនេះចេញ
														</a>
														<br />
														<a id="id-fullscreen" href="#" class="text-md text-dark-l2 d-inline-block mt-3">
																<i class="fa fa-expand text-110 text-green-m1 mr-1 w-2"></i>
																ធ្វើអោយពេញអេក្រង់
														</a>
														</div>
													</div>

													<div class="mt-auto mb-4 text-dark-tp2">
															HangKhmer Co.Ltd &copy; 2021
													</div>
												</div>
											</div>

											<div class="carousel-item minw-100 h-100">
												<!-- the second carousel item with dark background -->
												<div style="background-image: url({{asset('ace/assets/image/login-bg-2.svg')}});" class="d-flex flex-column align-items-center justify-content-start">
													<a class="mt-5 mb-2" href="{{route('home')}}">
														{{-- <i class="fa fa-leaf text-success-m2 fa-3x"></i> --}}
														<img height="90" src="{{asset('image/logo.png')}}" alt="">
													</a>
													<h2 class="text-blue-l1">
															ហាង <span class="text-110 text-white-tp3">ខ្មែរ</span>
													</h2>
												</div>
											</div>

											<div class="carousel-item minw-100 h-100">
													<div style="background-image: url({{asset('ace/assets/image/login-bg-3.jpg')}});" class="d-flex flex-column align-items-center justify-content-start">
													<div class="bgc-black-tp4 radius-1 p-3 w-90 text-center my-3 h-100">
															<a class="mt-5 mb-2" href="{{route('home')}}">
																	{{-- <i class="fa fa-leaf text-success-m2 fa-3x"></i> --}}
																	<img height="90" src="{{asset('image/logo.png')}}" alt="">
															</a>
															<h2 class="text-blue-l1">
															ហាង <span class="text-110 text-white-tp3">ខ្មែរ</span>
															</h2>
													</div>
													</div>
											</div>

											<div class="carousel-item minw-100 h-100">
													<div style="background-image: url({{asset('ace/assets/image/login-bg-4.jpg)')}};" class="d-flex flex-column align-items-center justify-content-start">
															<a class="mt-5 mb-2" href="{{route('home')}}">
																	{{-- <i class="fa fa-leaf text-success-m2 fa-3x"></i> --}}
																	<img height="90" src="{{asset('image/logo.png')}}" alt="">
															</a>
													<h2 class="text-blue-d1">
															ហាង <span class="text-110 text-dark-tp3">ខ្មែរ</span>
													</h2>
													</div>
											</div>
										</div>
									</div>
								</div>

								<div id="id-col-main" class="col-12 col-lg-7 py-lg-5 bgc-white px-0">
									<!-- you can also use these tab links -->
									<ul class="d-none mt-n4 mb-4 nav nav-tabs nav-tabs-simple justify-content-end bgc-black-tp11" role="tablist">
											<li class="nav-item mx-2">
											<a class="nav-link active px-2" data-toggle="tab" href="#id-tab-login" role="tab" aria-controls="id-tab-login" aria-selected="true">
													Login
											</a>
											</li>
											<li class="nav-item mx-2">
											<a class="nav-link px-2" data-toggle="tab" href="#id-tab-signup" role="tab" aria-controls="id-tab-signup" aria-selected="false">
													Signup
											</a>
											</li>
									</ul>

									<div class="tab-content tab-sliding border-0 p-0" data-swipe="right">

										<div class="tab-pane active show mh-100 px-3 px-lg-0 pb-3" id="id-tab-login">
											<!-- show this in desktop -->
											<div class="d-none d-lg-block col-md-6 offset-md-3 mt-lg-4 px-0">
													<h4 class="text-dark-tp4 border-b-1 brc-secondary-l2 pb-1 text-130">
													<i class="fa fa-coffee text-orange-m1 mr-1"></i>
													ស្វា​គម​ន៏​ការ​ត្រ​លប់​មក​វិញ
													</h4>
											</div>

											<!-- show this in mobile device -->
											<div class="d-lg-none text-secondary-m1 my-4 text-center">
													<a href="{{route('home')}}">
													<i class="fa fa-leaf text-success-m2 text-200 mb-4"></i>
													</a>
													<h1 class="text-170">
													<span class="text-blue-d1">
															កម្មវិធី <span class="text-80 text-dark-tp3">ហាងខ្មែរ</span>
													</span>
													</h1>
													ស្វា​គម​ន៏​ការ​ត្រ​លប់​មក​វិញ
											</div>

											@if (session('status'))
													<div class="mb-4 font-medium text-sm text-green-600">
															{{ session('status') }}
													</div>
											@endif

										{{-- Login --}}
											<form autocomplete="off" method="POST" action="{{ route('login') }}" class="form-row mt-4" >
													@csrf
													<div class="form-group col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-lg-6 offset-lg-3">
														<div class="d-flex align-items-center input-floating-label text-blue brc-blue-m2">
																<input placeholder="អ៊ីម៉ែល" type="text" name="email" :value="old('email')" required autofocus class="form-control form-control-lg pr-4 shadow-none" id="email" />
																<i class="fa fa-user text-grey-m2 ml-n4"></i>
																<label class="floating-label text-grey-l1 ml-n3" for="email">
																អ៊ីម៉ែល
																</label>
														</div>
													</div>

													<div class="form-group col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-lg-6 offset-lg-3 mt-2 mt-md-1">
														<div class="d-flex align-items-center input-floating-label text-blue brc-blue-m2">
																<input placeholder="លេខសំងាត់​" type="password"
																name="password" required autocomplete="current-password"
																class="form-control form-control-lg pr-4 shadow-none" id="id-login-password" />

																<i class="fa fa-key text-grey-m2 ml-n4"></i>
																<label class="floating-label text-grey-l1 ml-n3" for="id-login-password">
																		លេខសំងាត់​
																</label>
														</div>
													</div>

													<div class="col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-lg-6 offset-lg-3 text-right text-md-right mt-n2 mb-2">
															@if (Route::has('password.request'))
																	<a href="{{ route('password.request') }}" class="text-primary-m1 text-95" data-toggle="tab" data-target="#id-tab-forgot">
																			ភ្លេច​លេខសំងាត់​?
																	</a>
															@endif
													</div>

													<div class="form-group col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-lg-6 offset-lg-3">
														<label class="d-inline-block mt-3 mb-0 text-dark-l1">
																{{-- <input type="checkbox" class="mr-1" id="id-remember-me" id="remember_me" name="remember" /> --}}
																<input type="checkbox" class="mr-1" id="remember_me" name="remember" />
																ចងចាំខ្ញុំ
														</label>

														<button type="submit" class="btn btn-primary btn-block px-4 btn-bold mt-2 mb-4">
																ចូលប្រើ
														</button>
													</div>
											</form>

											<div class="form-row">
													<div class="col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-lg-6 offset-lg-3 d-flex flex-column align-items-center justify-content-center">
															<hr class="brc-default-l2 mt-0 mb-2 w-100" />
															<div class="p-0 px-md-2 text-dark-tp3 my-3">
																	មិនទាន់ជាសមាជិក?
																	<a class="text-success-m1 text-600 mx-1" data-toggle="tab" data-target="#id-tab-signup" href="#">
																	ចុះឈ្មោះឥលូវ
																	</a>
															</div>

															<hr class="brc-default-l2 w-100 mb-2" />
															<div class="mt-n4 bgc-white-tp2 px-3 py-1 text-secondary-d3 text-90">ឬចាប់ផ្តើមប្រើជាមួយ</div>

															<div class="my-2">
																	<a href="{{ url('auth/facebook') }}" class="btn btn-bgc-white btn-lighter-primary btn-h-primary btn-a-primary border-2 radius-round btn-lg mx-1">
																			<i class="fab fa-facebook-f text-110"></i>
																	</a>

																	<a href="{{ url('auth/github') }}" class="btn btn-bgc-white btn-lighter-info btn-h-info btn-a-info border-2 radius-round btn-lg px-25 mx-1">
																			<i class="fab fa-github text-110"></i>
																	</a>

																	<a href="{{ url('auth/google') }}" class="btn btn-bgc-white btn-lighter-red btn-h-red btn-a-red border-2 radius-round btn-lg px-25 mx-1">
																			<i class="fab fa-google text-110"></i>
																	</a>
															</div>

													</div>
											</div>

										</div>


										<div class="tab-pane mh-100 px-3 px-lg-0 pb-3" id="id-tab-signup" data-swipe-prev="#id-tab-login">
											<div class="position-tl ml-3 mt-3 mt-lg-0">
													<a href="#" class="btn btn-light-default btn-h-light-default btn-a-light-default btn-bgc-tp" data-toggle="tab" data-target="#id-tab-login">
													<i class="fa fa-arrow-left"></i>
													</a>
											</div>

											<!-- show this in desktop -->
											<div class="d-none d-lg-block col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-lg-6 offset-lg-3 mt-lg-4 px-0">
													<h4 class="text-dark-tp4 border-b-1 brc-grey-l1 pb-1 text-130">
													<i class="fa fa-user text-purple mr-1"></i>
													បង្កើត​គណនី
													</h4>
											</div>

											<!-- show this in mobile device -->
											<div class="d-lg-none text-secondary-m1 my-4 text-center">
													<i class="fa fa-leaf text-success-m2 text-200 mb-4"></i>
													<h1 class="text-170">
													<span class="text-blue-d1">កម្មវិធី <span class="text-80 text-dark-tp4">ហាងខ្មែរ</span></span>
													</h1>
													បង្កើត​គណនី
											</div>
										{{-- Register Form --}}
											<form autocomplete="off" class="form-row mt-4" method="POST" action="{{route('register')}}">
												@csrf
												<div class="form-group col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-lg-6 offset-lg-3 mt-1">
														<div class="d-flex align-items-center input-floating-label text-success brc-success-m2">
																<input placeholder="ឈ្មោះរបស់អ្នក"
																type="text" name="name" :value="old('name')" required autofocus autocomplete="name"
																class="form-control form-control-lg pr-4 shadow-none" id="id-signup-username" />
																<i class="fa fa-user text-grey-m2 ml-n4"></i>
																<label class="floating-label text-grey-l1 text-100 ml-n3" for="id-signup-username">
																		ឈ្មោះរបស់អ្នក
																</label>
														</div>
												</div>

												<div class="form-group col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-lg-6 offset-lg-3">
														<div class="d-flex align-items-center input-floating-label text-success brc-success-m2">
																<input placeholder="អ៊ីម៉ែល" type="email" name="email" :value="old('email')" required class="form-control form-control-lg pr-4 shadow-none" id="id-signup-email" />
																<i class="fa fa-envelope text-grey-m2 ml-n4"></i>
																<label class="floating-label text-grey-l1 text-100 ml-n3" for="id-signup-email">
																		អ៊ីម៉ែល
																</label>
														</div>
												</div>

												<div class="form-group col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-lg-6 offset-lg-3 mt-1">
														<div class="d-flex align-items-center input-floating-label text-success brc-success-m2">
																<input placeholder="ពាក្យសំង៉ាត់" type="password"
																name="password" required autocomplete="new-password"
																class="form-control form-control-lg pr-4 shadow-none" id="id-signup-password" />
																<i class="fa fa-key text-grey-m2 ml-n4"></i>
																<label class="floating-label text-grey-l1 text-100 ml-n3" for="id-signup-password">
																		ពាក្យសម្ងាត់
																</label>
														</div>
												</div>

												<div class="form-group col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-lg-6 offset-lg-3 mt-1">
														<div class="d-flex align-items-center input-floating-label text-success brc-success-m2">
																<input placeholder="Confirm Password"
																type="password" name="password_confirmation" required autocomplete="new-password"
																class="form-control form-control-lg pr-4 shadow-none" id="id-signup-password2" />
																<i class="fas fa-sync-alt text-grey-m2 ml-n4"></i>
																<label class="floating-label text-grey-l1 text-100 ml-n3" for="id-signup-password2">
																		បញ្ជាក់ពាក្យសម្ងាត់
																</label>
														</div>
												</div>

												<div class="form-group col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-lg-6 offset-lg-3 mt-1">
													<div class="d-flex align-items-center input-floating-label text-success brc-success-m2">
														<input placeholder="លេខទូរស័ព្ទ"
														type="telephone" name="phone" :value="old('phone')" required autofocus autocomplete="phone"
														class="form-control form-control-lg pr-4 shadow-none" id="phone" autocomplete="phone" />
														<i class="fa fa-phone text-grey-m2 ml-n4"></i>
														<label class="floating-label text-grey-l1 text-100 ml-n3" for="phone">
																លេខទូរស័ព្ទ
														</label>
													</div>
												</div>

												<div class="form-group col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-lg-6 offset-lg-3 mt-1">
													<div class="d-flex align-items-center input-floating-label text-success brc-success-m2">
														@php
															$provinces=DB::select('select * from provinces');
														@endphp
														{{-- <x-jet-input id="province_id" class="block mt-1 w-full" type="password" name="province_id" required autocomplete="province_id" /> --}}
														<select id="province_id"  class="form-control form-control-lg pr-4 shadow-none" name="province_id">
															@foreach ($provinces as $province)
																<option value="{{$province->id}}" >
																	{{$province->khmer_name}}
																</option>
															@endforeach
														</select>
													</div>
												</div>

												<div class="form-group col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-lg-6 offset-lg-3 mt-2">
														@if (Laravel\Jetstream\Jetstream::hasTermsAndPrivacyPolicyFeature())
																<label class="d-inline-block mt-3 mb-0 text-secondary-d2">
																		<input type="checkbox" class="mr-1" name="terms" id="terms" />
																		<span class="text-dark-m3">ខ្ញុំបានអានហើយយល់ព្រម <a  target="_blank" href="{{route('terms.show')}}" class="text-blue-d2">terms</a></span>
																		<span class="text-dark-m3">& គោលការណ៍​ភាព​ឯកជន <a  target="_blank" href="{{route('policy.show')}}" class="text-blue-d2">terms</a></span>
																</label>
														@endif
														<button type="submit" class="btn btn-success btn-block px-4 btn-bold mt-2 mb-3">
																ចុះឈ្មោះ
														</button>
												</div>
											</form>

											<div class="form-row w-100">
												<div class="col-12 col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-lg-6 offset-lg-3 d-flex flex-column align-items-center justify-content-center">
													<hr class="brc-default-l2 mt-0 mb-2 w-100" />

													<div class="p-0 px-md-2 text-dark-tp4 my-3">
															ជាសមាជិករួចហើយ?
															<a class="text-blue-d1 text-600 mx-1" data-toggle="tab" data-target="#id-tab-login" href="#">
																	ចូលប្រើ​ នៅទីនេះ
															</a>
													</div>

													<hr class="brc-default-l2 w-100 mb-2" />
													<div class="mt-n4 bgc-white-tp2 px-3 py-1 text-secondary-d3 text-90">ឬចុះឈ្មោះប្រើប្រាស់ ជាមួយ</div>

													<div class="mt-2 mb-3">
														<a href="{{ url('auth/facebook') }}" class="btn btn-bgc-white btn-lighter-primary btn-h-primary btn-a-primary border-2 radius-round btn-lg mx-1">
															<i class="fab fa-facebook-f text-110"></i>
														</a>

														<a href="{{ url('auth/github') }}" class="btn btn-bgc-white btn-lighter-info btn-h-info btn-a-info border-2 radius-round btn-lg px-25 mx-1">
																<i class="fab fa-github text-110"></i>
														</a>

														<a href="{{ url('auth/google') }}" class="btn btn-bgc-white btn-lighter-red btn-h-red btn-a-red border-2 radius-round btn-lg px-25 mx-1">
																<i class="fab fa-google text-110"></i>
														</a>
													</div>
												</div>
											</div>
										</div>

										<div class="tab-pane mh-100 px-3 px-lg-0 pb-3" id="id-tab-forgot" data-swipe-prev="#id-tab-login">
											<div class="position-tl ml-3 mt-2">
													<a href="#" class="btn btn-light-default btn-h-light-default btn-a-light-default btn-bgc-tp" data-toggle="tab" data-target="#id-tab-login">
													<i class="fa fa-arrow-left"></i>
													</a>
											</div>

											<div class="col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-lg-6 offset-lg-3 mt-5 px-0">
													<h4 class="pt-4 pt-md-0 text-dark-tp4 border-b-1 brc-grey-l2 pb-1 text-130">
															<i class="fa fa-key text-brown-m1 mr-1"></i>
															Recover Password
													</h4>
											</div>

											<form autocomplete="off" class="form-row mt-4" method="POST" action="{{ route('password.email') }}">
													@csrf
													<div class="form-group col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-lg-6 offset-lg-3">
															<label class="text-secondary-d3 mb-3" role="id-recover-email">
																	Enter your email address and we'll send you the instructions:
															</label>
															<div class="d-flex align-items-center">
																	<input type="email"
																	type="email" name="email" :value="old('email')" required autofocus
																	class="form-control form-control-lg pr-4 shadow-none"
																	id="id-recover-email" placeholder="Email" />
																	<i class="fa fa-envelope text-grey-m2 ml-n4"></i>
															</div>
													</div>

													<div class="form-group col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-lg-6 offset-lg-3 mt-1">
															<button type="submit" class="btn btn-orange btn-block px-4 btn-bold mt-2 mb-4">
																	Continue
															</button>
															{{-- <x-jet-button class="btn btn-orange btn-block px-4 btn-bold mt-2 mb-4">
																	{{ __('Email Password Reset Link') }}
															</x-jet-button> --}}
													</div>
											</form>

											<div class="form-row w-100">
													<div class="col-12 col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-lg-6 offset-lg-3 d-flex flex-column align-items-center justify-content-center">
													<hr class="brc-default-l2 mt-0 mb-2 w-100" />
													<div class="p-0 px-md-2 text-dark-tp4 my-3">
															<a class="text-blue-d1 text-600 btn-text-slide-x" data-toggle="tab" data-target="#id-tab-login" href="#">
															<i class="btn-text-2 fa fa-arrow-left text-110 align-text-bottom mr-2"></i>Back to Login
															</a>
													</div>
													</div>
											</div>
										</div>

									</div><!-- .tab-content -->
								</div>
							</div><!-- /.row -->
						</div><!-- /.col -->
					</div><!-- /.row -->

					<div class="d-lg-none my-3 text-white-tp1 text-center">
						<i class="fa fa-leaf text-success-l3 mr-1 text-110"></i> Sourng Company &copy; 2021
					</div>
				</div>
			</div>
		</div>
	</div>
</x-login-layout>
