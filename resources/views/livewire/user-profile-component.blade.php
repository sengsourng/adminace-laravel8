@push('custom_style')
   <link rel="stylesheet" type="text/css" href="{{asset('ace')}}/views/pages/page-profile/@page-style.css">
@endpush
<div>
	<div class="page-content container container-plus">
		<div class="row mt-2 mt-md-4">

				<!-- the left side profile picture and other info -->
			<div class="col-12 col-md-4">
				<div class="card acard">
					<div class="card-body">
						<span class="d-none position-tl mt-2 pt-3px">
						<span class="text-white bgc-blue-d1 ml-2 radius-b-1 py-2 px-2">
								<i class="fa fa-star"></i>
						</span>
						</span>
						<div class="d-flex flex-column py-3 px-lg-3 justify-content-center align-items-center">
								<div class="pos-rel">
										<img width="64" alt="Profile image" src="{{ Auth::user()->profile_photo_url }}" class="radius-round bord1er-2 brc-warning-m1">
										<span class=" position-tr bgc-success p-1 radius-round border-2 brc-white mt-2px mr-2px"></span>
								</div>

								<div class="text-center mt-2">
										<h5 class="text-130 text-dark-m3">
										{{$this->name}}
										</h5>

										<span class="text-80 text-primary text-600">
												{{-- {{ Auth::user()->currentTeam->name }} --}}
										</span>

										<span class="d-none badge bgc-orange-l3 text-orange-d3 pt-2px pb-1 text-85 radius-round px-25 border-1 brc-orange-m3">
												pro
										</span>
								</div>

							<div class="mx-auto mt-25 text-center">
								<i class="fa fa-star text-orange"></i>
								<i class="fa fa-star text-orange"></i>
								<i class="fa fa-star text-orange"></i>
								<i class="fa fa-star text-orange"></i>
								<i class="fa fa-star-half-alt text-orange"></i>
							</div>

							<hr class="w-90 mx-auto brc-secondary-l3">

							<div class="text-center">
								<button type="button" class="btn btn-blue pos-rel px-5 px-md-4 px-lg-5">
									<i class="far fa-handshake mr-15 text-110"></i>
									Hire Me
								</button>
							</div>

							<hr class="w-90 mx-auto mb-1 brc-secondary-l3">

							<div class="mt-3">
								<a href="#" class="btn btn-white btn-text-green btn-h-green btn-a-green radius-1 py-2 px-1 shadow-sm">
									<i class="far fa-envelope w-4 text-120"></i>
								</a>

								<a href="#" class="btn btn-white btn-text-info btn-h-info btn-a-info radius-1 py-2 px-1 shadow-sm">
									<i class="fab fa-github w-4 text-120"></i>
								</a>

								<a href="#" class="btn btn-white btn-text-red btn-h-red btn-a-red radius-1 py-2 px-1 shadow-sm">
									<i class="fab fa-pinterest-p w-4 text-120"></i>
								</a>
							</div>

							<hr class="w-90 mx-auto mb-1 brc-secondary-l3">

							<div class="row w-100 text-center">
								<div class="col-4">
									<div class="px-1 pt-2">
										<span class="text-150 text-dark-m3">15</span>
										<br>
										<span class="text-grey-m1 text-90">photos</span>
									</div>

									<div class="position-rc h-75 border-l-1 brc-secondary-l3"></div>
								</div>

								<div class="col-4">
									<div class="px-1 pt-2">
										<span class="text-150 text-dark-m3">132</span>
										<br>
										<span class="text-grey-m1 text-90">followers</span>
									</div>

									<div class="position-rc h-75 border-l-1 brc-secondary-l3"></div>
								</div>

								<div class="col-4">
									<div class="px-1 pt-2">
										<span class="text-150 text-dark-m3">141</span>
										<br>
										<span class="text-grey-m1 text-90">likes</span>
									</div>
								</div>
							</div>
							<hr class="w-90 mx-auto mb-1 border-dotted">
						</div><!-- /.d-flex -->
					</div><!-- /.card-body -->
				</div><!-- /.card -->

				<div class="mt-3 ccard px-4 py-3">
					<div class="position-tl w-100 border-t-4 brc-blue-tp3 radius-2"></div>
					<div class="d-flex align-items-start">
						<h3 class="text-orange-d3 text-140 mb-2">
								កែសម្រួលព័ត៌មាន:
						</h3>
					</div>

					<div class="d-flex flex-wrap">
						<div class="tab-pane px-1 px-md-2 px-lg-4" id="profile-tab-edit">
								<div class="row">
									<div class="col-md-12 col-lg-12  mt-3">

										@if (Session::has('message'))
											<div class="alert alert-success" role="alert" >{{Session::get('message')}}</div>
										@endif

										<form class="text-grey-d1 text-95" autocomplete="off" wire:submit.prevent="updateProfile" >

											<div class="form-group row mx-0">
												<div class="offset-xl-3 col-sm-8 col-lg-6">
													<div class="col-sm-9 text-secondary">
														<input wire:model.defer="newimage" name="profile_photo_path" type='file' id="newimage" class="form-control-file" style="display:none">
															{{-- <input id="newimage" type="file" class="form-control-file" name="image" wire:model.defer="newimage"> --}}
															@error('newimage') <p class="text-danger">{{$message}}</p> @enderror
															@if ($newimage)
																<img  class="rounded-squre p-1 bg-primary" width="110" src="{{$newimage->temporaryUrl()}}" alt="" style="cursor: pointer;" onclick="document.getElementById('newimage').click()">
															@else
																@if (Laravel\Jetstream\Jetstream::managesProfilePhotos())
																	<img src="{{ Auth::user()->profile_photo_url }}" alt="{{Auth::user()->name}}"
																	class="rounded float-start p-1 bg-primary" width="110"
																	style="cursor: pointer;" onclick="document.getElementById('newimage').click()">
																@else
																	<div class="rounded pull-left">
																		<img  src="{{asset('storage/'.$profile_photo_path)}}" alt="{{$name}}"
																				style="cursor: pointer;" onclick="document.getElementById('newimage').click()"
																				class="rounded-squre p-1 bg-primary" width="210">
																	</div>
																@endif
															@endif
														</div>
												</div>
											</div>

											<div class="row">
												<div class="form-group row mx-0">
														<label for="id-field1" class="col-sm-12 col-xl-12 col-form-label text-sm-left">Full Name</label>
														<div class="col-sm-12 col-lg-12">
															<input type="text" wire:model.defer="name" class="form-control brc-on-focus brc-success-m2" id="id-field1" placeholder="e.g. Alex Doe" />
														</div>
													</div>
													<div class="form-group row mx-0">
														<label for="id-field2" class="col-sm-4 col-xl-12 col-form-label text-sm-left">Email Address</label>
														<div class="col-sm-12 col-lg-12">
															<input type="email" name="email" wire:model.defer="email" class="form-control brc-on-focus brc-success-m2" id="id-field2" placeholder="Enter your email address here" />
														</div>
													</div>


													<div class="form-group row  mx-0">
														<label for="id-field3" class="col-sm-12 col-xl-12 col-form-label text-sm-left">Phone Number</label>
														<div class="col-sm-12 col-lg-12">
															<input type="text" name="phone" wire:model.defer="phone" class="form-control brc-on-focus brc-success-m2" id="id-field3" placeholder="e.g. (+44) 3482342" />
														</div>
													</div>

													<div class="form-group row mx-0">
														<label for="id-field4" class="col-sm-12 col-xl-12 col-form-label text-sm-left">Address</label>
														<div class="col-sm-12 col-lg-12 col-md-12">
															<input type="text" name="address" wire:model.defer="address" class="form-control brc-on-focus brc-success-m2" id="id-field4" placeholder="" />
														</div>
													</div>

													<div class="form-group row mx-0">
														<label for="id-field4" class="col-sm-12 col-xl-12 col-form-label text-sm-left">ខេត្ត/ក្រុង</label>
														<div class="col-sm-12 col-lg-12">
																<select name="province_id" id="province_id" class="form-control" wire:model.defer="province_id">
																		<option value="">ជ្រើសរើស ខេត្តក្រុង</option>
																		@foreach ($provinces as $province)
																				<option  value="{{$province->id}}">{{$province->khmer_name}} ( <span class="text-danger">{{$province->name}}</span> )</option>
																		@endforeach
																</select>
														</div>
													</div>
											</div>

										</form>

									</div>

									<div class="col-md-12">
										<hr class="border-double brc-dark-l3" />

										<div class="form-group text-center mt-4 mb-3">
											{{-- <button type="button" class="btn btn-outline-secondary radius-1 px-3 mx-1">
												Cancel
											</button> --}}
											<input type="button" wire:click="updateProfile()" class="btn btn-outline-green radius-1 px-4 mx-1" value="Save Changes">
										</div>
									</div>

								</div>
							</div><!-- /.tab-pane -->
					</div>
				</div>
			</div><!-- .col -->


			<!-- the right side profile tabs -->
			<div class="col-12 col-md-8 mt-3 mt-md-0">
				<div class="card dcard h-100">
					<div class="card-body p-0">
						<div class="sticky-nav"><div class="sticky-trigger"></div>
							<div class="position-tr w-100 border-t-4 brc-blue-m2 radius-2 d-md-none"></div>

							<ul id="profile-tabs" class="nav nav-tabs-scroll is-scrollable nav-tabs nav-tabs-simple p-1px pl-25 bgc-secondary-l4 border-b-1 brc-dark-l3 radius-t-1" role="tablist">
								<li class="nav-item mr-2 mr-lg-3">
									<a class="d-style nav-link px-2 py-35 brc-primary-tp1 active" data-toggle="tab" href="#profile-tab-overview" role="tab" aria-controls="profile-tab-overview" aria-selected="true">
										<span class="d-n-active text-dark-l1">1. ទិដ្ឋភាពទូទៅ</span>
										<span class="d-active text-dark-m3">1. ទិដ្ឋភាពទូទៅ</span>
									</a>
								</li>

								<li class="nav-item mr-2 mr-lg-3">
									<a class="d-style nav-link px-2 py-35 brc-primary-tp1 pr-45" data-toggle="tab" href="#profile-tab-activity" role="tab" aria-controls="profile-tab-activity" aria-selected="false">
										<span class="d-n-active text-dark-l1">2. ប្តូរពាក្យសំង៉ាត់</span>
										<span class="d-active text-dark-m3">2. ប្តូរពាក្យសំង៉ាត់</span>
										<span class="pos-abs ml-15 badge bgc-red-d2 text-white radius-3 text-85">5</span>
									</a>
								</li>

								<li class="nav-item mr-2 mr-lg-3">
									<a class="d-style nav-link px-2 py-35 brc-primary-tp1" data-toggle="tab" href="#profile-tab-timeline" role="tab" aria-controls="profile-tab-timeline" aria-selected="false">
										<span class="d-n-active text-dark-l1">3. ការកំណត់ពេលវេលា</span>
										<span class="d-active text-dark-m3">3. ការកំណត់ពេលវេលា</span>
									</a>
								</li>

							</ul>
						</div><!-- /.sticky-nav-md -->


						<div class="tab-content px-0 tab-sliding flex-grow-1 border-0">

							<!-- overview tab -->
							<div class="tab-pane show px-1 px-md-2 px-lg-4 active" id="profile-tab-overview">
								<div class="row mt-1">
									<div class="col-12 px-4">
										<!-- infobox -->
										<div class="d-flex justify-content-center my-3 flex-wrap flex-equal">
											<div class="dcard bgc-h-primary-l4 brc-h-primary-m4 px-3 py-2 rounded text-center mx-1 mb-1">
												<span class="text-170 text-dark-tp3"><span class="pos-abs ml-n3">~</span>48k</span>
												<br>
												<span class="text-90 text-dark-tp2">
																		Views
																</span>
											</div>

											<div class="dcard bgc-h-primary-l4 brc-h-primary-m4 px-3 py-2 rounded text-center mx-1 mb-1">
												<span class="text-170 text-dark-tp3">132</span>
												<br>
												<span class="text-90 text-dark-tp2">
																		Followers
																</span>
											</div>

											<div class="dcard bgc-h-primary-l4 brc-h-primary-m4 px-3 py-2 rounded text-center mx-1 mb-1">
												<span class="text-170 text-dark-tp3">9</span>
												<br>
												<span class="text-90 text-dark-tp2">
																		Projects
																</span>
											</div>

											<div class="dcard bgc-h-primary-l4 brc-h-primary-m4 py-2 rounded text-center mx-1 mb-1">
												<span class="text-170 text-primary-d1"><span class="pos-abs ml-n3">~</span>11k</span>
												<br>
												<span class="text-90 text-dark-tp2">
																		Revenue
																</span>
											</div>
										</div>
									</div>


									<div class="col-12 px-4 mt-3">

										<h4 class="mt-2 text-dark-m3 text-130">
											<i class="fa fa-pen-alt text-85 text-purple-d1 w-3"></i>
											អំពីខ្ញុំ
										</h4>

										<div class="d-flex flex-column flex-sm-row align-items-center align-items-sm-start mt-3 mb-2 text-95 pl-3">
											<div class="d-style order-first order-sm-last mx-2 radius-2 overflow-hidden mb-3 mb-sm-0">
												<img alt="Amy's avatar" src="{{ Auth::user()->profile_photo_url }}" class="opacity-2 d-zoom-2" height="150">
											</div>

											<div class="mt-2 mt-sm-0 flex-grow-1 text-dark-m2">
												<p class="mb-1">
													សួស្តី, ខ្ញុំបាទឈ្មោះ សេង ស៊ង់. I'm a professional designer based in Dublin.
												</p>
												<p class="mb-1">
													My job is mostly lorem ipsuming and dolor sit ameting for clients!
												</p>
												<p>
													As long as consectetur adipiscing elit...
													<a href="#" class="text-blue-d1 text-600">Read more</a>
												</p>
											</div>
										</div>
									</div>


									<div class="col-12 px-4 mt-3">
										<h4 class="text-dark-m3 text-140">
											<i class="far fa-lightbulb text-danger-d1 text-85 w-3"></i>
											ជំនាញរបស់ខ្ញុំ​-My Skills
										</h4>

										<div id="skill-chart" class="d-flex justify-content-center mx-auto flex-wrap"><div class="pos-rel m-2 text-center text-green-m1" style="max-width: 30%;"><div class="chartjs-size-monitor"><div class="chartjs-size-monitor-expand"><div class=""></div></div><div class="chartjs-size-monitor-shrink"><div class=""></div></div></div>            <canvas height="100" width="100" style="display: block;" class="chartjs-render-monitor"></canvas>            <span class="position-center text-85 text-600 text-secondary-d3">HTML &amp; CSS</span>        </div><div class="pos-rel m-2 text-center text-purple-m1" style="max-width: 30%;"><div class="chartjs-size-monitor"><div class="chartjs-size-monitor-expand"><div class=""></div></div><div class="chartjs-size-monitor-shrink"><div class=""></div></div></div>            <canvas height="100" width="100" class="chartjs-render-monitor" style="display: block;"></canvas>            <span class="position-center text-85 text-600 text-secondary-d3">Angular</span>        </div><div class="pos-rel m-2 text-center text-primary-m1" style="max-width: 30%;"><div class="chartjs-size-monitor"><div class="chartjs-size-monitor-expand"><div class=""></div></div><div class="chartjs-size-monitor-shrink"><div class=""></div></div></div>            <canvas height="100" width="100" class="chartjs-render-monitor" style="display: block;"></canvas>            <span class="position-center text-85 text-600 text-secondary-d3">Backend</span>        </div></div>

										<div class="mt-2 text-center">
											<span class="d-inline-block radius-round bgc-blue-l2 text-dark-tp3 text-90 px-25 py-3px mx-2px my-2px">
																Node.js
														</span>
											<span class="d-inline-block radius-round bgc-purple-l2 text-dark-tp3 text-90 px-25 py-3px mx-2px my-2px">
																Docker
														</span>
											<span class="d-inline-block radius-round bgc-success-l2 text-dark-tp3 text-90 px-25 py-3px mx-2px my-2px">
																Jenkins
														</span>
											<span class="d-inline-block radius-round bgc-default-l2 text-dark-tp3 text-90 px-25 py-3px mx-2px my-2px">
																Database
														</span>
											<span class="d-inline-block radius-round bgc-warning-l2 text-dark-tp3 text-90 px-25 py-3px mx-2px my-2px">
																Agile
														</span>
											<span class="d-inline-block radius-round bgc-purple-l2 text-dark-tp3 text-90 px-25 py-3px mx-2px my-2px">
																VCS
														</span>
										</div>
									</div>
								</div>


								<div class="row mt-5">
									<div class="col-12 px-4 mb-3">

										<h4 class="text-dark-m3 text-140">
											<i class="fa fa-info text-blue mr-1 w-2"></i>
											ពត៌មានផ្សេងៗ-More Info
										</h4>

										<hr class="w-100 mx-auto mb-0 brc-default-l2">

										<div class="bgc-white radius-1">
											<table class="table table table-striped-default table-borderless">
												<tbody><tr>
													<td>
														<i class="far fa-user text-success"></i>
													</td>

													<td class="text-95 text-600 text-secondary-d2">
														ឈ្មោះរបស់អ្នក
													</td>

													<td class="text-dark-m3">
														{{$this->name}}
													</td>
												</tr>

												<tr>
													<td>
														<i class="far fa-envelope text-blue"></i>
													</td>

													<td class="text-95 text-600 text-secondary-d2">
														អ៊ីម៉ែល-Email
													</td>

													<td class="text-blue-d1 text-wrap">
														{{$this->email}}
													</td>
												</tr>

												<tr>
													<td>
														<i class="fa fa-phone text-purple"></i>
													</td>

													<td class="text-95 text-600 text-secondary-d2">
														ទូរស័ព្ទ/Phone
													</td>

													<td class="text-dark-m3">
														{{$this->phone}}
													</td>
												</tr>

												<tr>
													<td>
														<i class="fa fa-map-marker text-orange-d1"></i>
													</td>

													<td class="text-95 text-600 text-secondary-d2">
														ខេត្ត/ក្រុង - Province
													</td>

													<td class="text-dark-m3">
														{{Auth::user()->province->khmer_name??null}}
													</td>
												</tr>

												<tr>
													<td>
														<i class="far fa-clock text-secondary"></i>
													</td>

													<td class="text-95 text-600 text-secondary-d2">
														អាស័យដ្ឋាន
													</td>

													<td class="text-dark-m3">
														{{$address}}
													</td>
												</tr>

												<tr>
														<td>
															<i class="far fa-clock text-secondary"></i>
														</td>

														<td class="text-95 text-600 text-secondary-d2">
															តួនាទីប្រើប្រាស់
														</td>

														<td class="text-dark-m3">
															{{-- {{Auth::user()->role->name}} --}}
																@foreach(Auth::user()->roles as $key => $entry)
																		{{ $entry->title }}
																@endforeach
														</td>
													</tr>
											</tbody></table>
										</div>

									</div>

								</div><!-- /.row -->
							</div><!-- /.tab-pane -->
							<!-- activity tab -->
							<div class="tab-pane px-1 px-md-2 px-lg-3" id="profile-tab-activity">
								<div>
									<div class="d-flex m-3">
										<h4 class="text-dark-tp4 text-130 p-0 m-0">
											ប្តូរពាក្យសំង៉ាត់
										</h4>
									</div>

									<hr class="border-dotted mx-3">
									<div class="px-lg-3">
										<div class="d-flex flex-wrap">


														<div class="row">
														<div class="col-12 col-lg-10 offset-lg-1 mt-3">
																@if (Session::has('message'))
																		<div class="alert alert-success" role="alert" >{{Session::get('message')}}</div>
																@endif
																<form class="text-grey-d1 text-95" autocomplete="off" wire:submit.prevent="updatePassword">


																<div class="row">
																		<div class="form-group row mx-0">
																				<label for="current_password" class="col-sm-12 col-xl-12 col-form-label text-sm-left">Current password</label>
																				<div class="col-sm-12 col-lg-12">
																				<input type="password" wire:model.defer="current_password" class="form-control brc-on-focus brc-success-m2" id="current_password" placeholder="Old Password" />
																				</div>
																		</div>

																		<div class="form-group row mx-0">
																				<label for="password" class="col-sm-4 col-xl-12 col-form-label text-sm-left">Password</label>
																				<div class="col-sm-12 col-lg-12">
																				<input type="password" name="password" wire:model.defer="password" class="form-control brc-on-focus brc-success-m2" id="password" placeholder="Enter new Password" />
																				</div>
																		</div>

																		<div class="form-group row mx-0">
																				<label for="password_confirmation" class="col-sm-4 col-xl-12 col-form-label text-sm-left">Password confirmation</label>
																				<div class="col-sm-12 col-lg-12">
																				<input type="password" name="password_confirmation" wire:model.defer="password_confirmation" class="form-control brc-on-focus brc-success-m2" id="password_confirmation" placeholder="Enter your confirm password" />
																				</div>
																		</div>



																</div>

																</form>

														</div>



														<div class="col-12">
																<hr class="border-double brc-dark-l3" />

																<div class="form-group text-center mt-4 mb-3">
																<button type="button" class="btn btn-outline-secondary radius-1 px-3 mx-1">
																		Cancel
																</button>
																<input type="button" wire:loading.attr="disabled" wire:click="updatePassword()" class="btn btn-outline-green radius-1 px-4 mx-1" value="Save Changes">
																</div>
														</div>

														</div>




										</div>
									</div>
								</div>
							</div><!-- /.tab-pane -->



							<!-- timeline tab -->
							<div class="tab-pane px-1 px-md-2 px-lg-3" id="profile-tab-timeline">
								<div class="px-3 text-grey-m1 text-95">

									<div class="mt-4 mb-2">
										<span class="badge badge-info ml-n1">Today</span>
									</div>

									<div class="mt-1 pl-1 pos-rel">
										<div class="position-tl h-90 border-l-2 brc-secondary-l1 ml-2 ml-lg-25 mt-2"></div>

										<div class="row pos-rel">
											<div class="position-tl mt-1 ml-25 w-2 h-2 bgc-white radius-round border-3 brc-success-m1"></div>

											<div class="col-4 ml-4 col-lg-3 ml-lg-0 text-90 text-grey-m2 text-left text-lg-center">10:10 pm</div>
											<div class="col-12 ml-4 col-lg-9 ml-lg-n4 pb-2 border-b-1 brc-grey-l4">
												<i class="fa fa-bookmark text-success-m3 w-2 text-center mr-1"></i>
												<a href="#" class="text-primary-d1 text-600">
													Alex
												</a>

												<span class="text-dark-m3">
																				bookmarked your post
																		</span>

											</div>
										</div>
										<div class="row pos-rel my-3">
											<div class="position-tl mt-1 ml-25 w-2 h-2 bgc-white radius-round border-3 brc-blue-m1"></div>

											<div class="col-4 ml-4 col-lg-3 ml-lg-0 text-90 text-grey-m2 text-left text-lg-center">9:35 pm</div>
											<div class="col-12 ml-4 col-lg-9 ml-lg-n4 pb-2 border-b-1 brc-grey-l4">
												<i class="fa fa-pen text-blue-m2 w-2 text-center mr-1"></i>
												<a href="#" class="text-primary-d1 text-600">
													Susan
												</a>

												<span class="text-dark-m3">
																				reviewed a product
																		</span>

												<a href="#" class="text-blue-d2">
													Read
												</a>
											</div>
										</div>
										<div class="row pos-rel my-3">
											<div class="position-tl mt-1 ml-25 w-2 h-2 bgc-white radius-round border-3 brc-purple-m1"></div>

											<div class="col-4 ml-4 col-lg-3 ml-lg-0 text-90 text-grey-m2 text-left text-lg-center">4:19 pm</div>
											<div class="col-12 ml-4 col-lg-9 ml-lg-n4 pb-2 border-b-1 brc-grey-l4">
												<i class="fa fa-book text-purple w-2 text-center mr-1"></i>
												<a href="#" class="text-primary-d1 text-600">
													John
												</a>

												<span class="text-dark-m3">
																				started a new course
																		</span>

												<a href="#" class="text-blue-d2">
													Enroll
												</a>
											</div>
										</div>
										<div class="row pos-rel my-3">
											<div class="position-tl mt-1 ml-25 w-2 h-2 bgc-white radius-round border-3 brc-warning-m1"></div>

											<div class="col-4 ml-4 col-lg-3 ml-lg-0 text-90 text-grey-m2 text-left text-lg-center">11:40 am</div>
											<div class="col-12 ml-4 col-lg-9 ml-lg-n4 pb-2 border-b-1 brc-grey-l4">
												<i class="fa fa-image text-warning w-2 text-center mr-1"></i>
												<a href="#" class="text-primary-d1 text-600">
													Alex
												</a>

												<span class="text-dark-m3">
																				posted a new photo
																		</span>

												<a href="#" class="text-blue-d2">
													View
												</a>
											</div>
										</div>
										<div class="row pos-rel my-3">
											<div class="position-tl mt-1 ml-25 w-2 h-2 bgc-white radius-round border-3 brc-pink-m1"></div>

											<div class="col-4 ml-4 col-lg-3 ml-lg-0 text-90 text-grey-m2 text-left text-lg-center">9:00 am</div>
											<div class="col-12 ml-4 col-lg-9 ml-lg-n4 pb-2 border-b-1 brc-grey-l4">
												<i class="fa fa-user text-pink w-2 text-center mr-1"></i>
												<a href="#" class="text-primary-d1 text-600">
													Max
												</a>

												<span class="text-dark-m3">
																				became friends with you
																		</span>

											</div>
										</div>

									</div>

									<div class="mt-4 mb-2">
										<span class="badge badge-secondary ml-n1">Yesterday</span>
									</div>

									<div class="mt-1 pl-1 pos-rel">
										<div class="position-tl h-90 border-l-2 brc-secondary-l1 ml-2 ml-lg-25 mt-2"></div>

										<div class="row pos-rel">
											<div class="position-tl mt-1 ml-25 w-2 h-2 bgc-white radius-round border-3 brc-purple-m1"></div>

											<div class="col-4 ml-4 col-lg-3 ml-lg-0 text-90 text-grey-m2 text-left text-lg-center">6:11 pm</div>
											<div class="col-12 ml-4 col-lg-9 ml-lg-n4 pb-2 border-b-1 brc-grey-l4">
												<i class="fa fa-microphone text-purple-m1 w-2 text-center mr-1"></i>
												<a href="#" class="text-primary-d1 text-600">
													Freddie
												</a>

												<span class="text-dark-m3">
																				sang a new song
																		</span>

												<a href="#" class="text-blue-d2">
													Listen
												</a>
											</div>
										</div>
										<div class="row pos-rel my-3">
											<div class="position-tl mt-1 ml-25 w-2 h-2 bgc-white radius-round border-3 brc-success-m1"></div>

											<div class="col-4 ml-4 col-lg-3 ml-lg-0 text-90 text-grey-m2 text-left text-lg-center">5:35 pm</div>
											<div class="col-12 ml-4 col-lg-9 ml-lg-n4 pb-2 border-b-1 brc-grey-l4">
												<i class="fa fa-pen text-success-m3 w-2 text-center mr-1"></i>
												<a href="#" class="text-primary-d1 text-600">
													Robin
												</a>

												<span class="text-dark-m3">
																				read a new novel
																		</span>

											</div>
										</div>
										<div class="row pos-rel my-3">
											<div class="position-tl mt-1 ml-25 w-2 h-2 bgc-white radius-round border-3 brc-blue-m1"></div>

											<div class="col-4 ml-4 col-lg-3 ml-lg-0 text-90 text-grey-m2 text-left text-lg-center">4:20 pm</div>
											<div class="col-12 ml-4 col-lg-9 ml-lg-n4 pb-2 border-b-1 brc-grey-l4">
												<i class="fa fa-graduation-cap text-blue-m1 w-2 text-center mr-1"></i>
												<a href="#" class="text-primary-d1 text-600">
													Katy
												</a>

												<span class="text-dark-m3">
																				finished a long course
																		</span>

											</div>
										</div>
										<div class="row pos-rel my-3">
											<div class="position-tl mt-1 ml-25 w-2 h-2 bgc-white radius-round border-3 brc-warning-m1"></div>

											<div class="col-4 ml-4 col-lg-3 ml-lg-0 text-90 text-grey-m2 text-left text-lg-center">10:15 am</div>
											<div class="col-12 ml-4 col-lg-9 ml-lg-n4 pb-2 border-b-1 brc-grey-l4">
												<i class="fa fa-user text-warning w-2 text-center mr-1"></i>
												<a href="#" class="text-primary-d1 text-600">
													Philip
												</a>

												<span class="text-dark-m3">
																				update profile photo
																		</span>

												<a href="#" class="text-blue-d2">
													View
												</a>
											</div>
										</div>
										<div class="row pos-rel my-3">
											<div class="position-tl mt-1 ml-25 w-2 h-2 bgc-white radius-round border-3 brc-pink-m1"></div>

											<div class="col-4 ml-4 col-lg-3 ml-lg-0 text-90 text-grey-m2 text-left text-lg-center">9:35 am</div>
											<div class="col-12 ml-4 col-lg-9 ml-lg-n4 pb-2 border-b-1 brc-grey-l4">
												<i class="fa fa-plane text-pink w-2 text-center mr-1"></i>
												<a href="#" class="text-primary-d1 text-600">
													Melissa
												</a>

												<span class="text-dark-m3">
																				is going on vacation
																		</span>

											</div>
										</div>

									</div>
								</div>
							</div><!-- /.tab-pane -->





						</div><!-- /.tab-content -->

					</div>
				</div><!-- /.card -->

			</div>

		</div>
	</div>
</div>

@push('modals')
    <script src="{{asset('ace')}}/views/pages/page-profile/@page-script.js"></script>
@endpush
