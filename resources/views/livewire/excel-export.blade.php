<a wire:click="export" class="dropdown-item " wire:loading.attr="disabled" style="cursor:pointer;">
    <i wire:loading class="fas fa-spinner fa-spin"></i>
    នាំចេញជា {{ $format }}
</a>
