<div>
	<div class="row">
		<div class="col-lg-4">
			<div class="page-header">
				<h5 class="page-title text-primary-d2">
					<a href="{{ route('admin.dashboard') }}"><i class="fa fa-home text-80"></i></a>
						<span class="page-info text-secondary-d2 text-80">
								<i class="fa fa-angle-double-right text-80"></i>
								<a href="{{ route('admin.users.index') }}">{{ trans('cruds.user.title') }}</a>
						</span>
						<span class="page-info text-secondary-d2 text-80">
								<i class="fa fa-angle-double-right text-80"></i>
								{{ trans('global.edit') }} {{ trans('cruds.user.title_singular') }}
						</span>
				</h5>
			</div>
		</div>
	</div>

	<div class="row justify-content-center">
			<div class="col-lg-6">

					<div class="card border radius-0">
							<div class="card-header bgc-info-d1 text-white">
									<h5 class="card-title text-white">
											<i class="fa fa-table mr-2px"></i>
											{{ trans('global.edit') }} {{ trans('cruds.user.title_singular') }}
									</h5>
									<div class="mb-2 mb-sm-0">
											<a href="{{ route('admin.users.index') }}" class="btn btn-green px-3 d-block w-100 text-95 radius-round border-2 brc-black-tp10">
													<i class="fa fa-list mr-1"></i>
													{{ trans('global.list') }} <span class="d-sm-none d-md-inline">{{ trans('cruds.user.title') }}</span>
											</a>
									</div>
							</div>

							<form wire:submit.prevent="submit" class="pt-3" action="#" id="frmObject" method="POST" enctype="multipart/form-data">
									@csrf
									<div class="card-body bg-white p-0 collapse show" style="">
										<div class="p-3">
											<div class="form-group mb-2">
												<label class="form-label required" for="name">{{ trans('cruds.user.fields.name') }} <span class="text-danger">*</span>
													@error('user.name') <span class="text-danger text-sm">{{ $message }}</span> @enderror
												</label>
												<input wire:model.defer="user.name" class="form-control" type="text" name="name" id="name">
											</div>
											<div class="form-group mb-2 {{ $errors->has('user.email') ? 'invalid' : '' }}">
												<label class="form-label required" for="email">{{ trans('cruds.user.fields.email') }} <span class="text-danger">*</span>
													@error('user.email') <span class="text-danger text-sm">{{ $message }}</span> @enderror
												</label>
												<input wire:model.defer="user.email" class="form-control" type="email" name="email" id="email" >
											</div>
											<div class="form-group mb-2 {{ $errors->has('password') ? 'invalid' : '' }}">
													<label class="form-label required" for="password">{{ trans('cruds.user.fields.password') }}
														@error('password') <span class="text-danger text-sm">{{ $message }}</span> @enderror
													</label>
													<input wire:model.defer="password" class="form-control" type="password" name="password" id="password" >
											</div>
											<div class="form-group mb-2 {{ $errors->has('password-confirm') ? 'invalid' : '' }}">
												<label class="form-label required" for="password_confirmation">{{ trans('cruds.user.fields.password_confirmation') }}</label>
												<input wire:model.defer="password_confirmation" class="form-control" type="password" name="password-confirm" id="password-confirm" autocomplete="new-password">
											</div>
											<div class="form-group mb-2 {{ $errors->has('roles') ? 'invalid' : '' }}" wire:ignore>
												<div id="select2-parent">
													<label class="form-label required" for="roles">{{ trans('cruds.user.fields.roles') }} <span class="text-danger">*</span>
														@error('roles') <span class="text-danger text-sm">{{ $message }}</span> @enderror
													</label>
													<select wire:model.defer="roles" wire:ignore multiple class="form-control user-roles" id="roles" name="roles" style="width: 100%" data-placeholder="Choose {{ trans('cruds.user.fields.roles') }}">
														@foreach ($listsForFields['roles'] as $key => $item)
															<option value="{{ $key }}">{{  $item }}</option>
														@endforeach
													</select>
												</div>
											</div>
										</div>
									</div><!-- /.card-body -->
									<div class="card-footer bgc-white px-4 py-3 brc-dark-l3 d-flex justify-content-center">
											<a href="{{ route('admin.users.index') }}" class="btn border-2 btn-lighter-secondary btn-text-black btn-h-secondary btn-a-secondary btn-bgc-tp">
												<i class="fa fa-times text-110 text-danger-m1 mr-1"></i>
												{{ trans('global.cancel') }}
											</a>
											&nbsp;&nbsp;&nbsp;
											<button type="submit" class="btn border-2 btn-lighter-primary btn-text-black btn-h-primary btn-a-primary btn-bgc-tp">
												<i class="fa fa-check mr-1 text-success"></i>
												{{ trans('global.save') }}
											</button>
									</div><!-- /.card-footer -->
							</form>
					</div> <!--card-->

			</div>
	</div>
</div>

@push('scripts')
	<script>
		$(document).ready(function() {
			$('.user-roles').on('change', function (e) {
				var data = $('.user-roles').select2("val");
				console.log(data)
				@this.set('roles', data);
			});

			$('.user-roles').select2({
				dropdownParent: $('#select2-parent'),
				width: $(this).data('width') ? $(this).data('width') : $(this).hasClass('w-100') ? '100%' : 'style',
				placeholder: $(this).data('placeholder'),
				allowClear: Boolean($(this).data('allow-clear')),
			});
		});
	</script>
@endpush
