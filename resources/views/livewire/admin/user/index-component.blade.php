<div>
	<div class="row">
		<div class="col-lg-4">
				<div class="page-header">
						<h5 class="page-title text-primary-d2">
							<a href="{{ route('admin.dashboard') }}"><i class="fa fa-home text-80"></i> Home</a>
								<span class="page-info text-secondary-d2 text-80">
										<i class="fa fa-angle-double-right text-80"></i>
										{{ trans('cruds.user.title_singular') }} {{ trans('global.list') }}
								</span>
						</h5>
				</div>
		</div>
	</div>

	<div class="row mt-3">
        <div class="col-12">
                <div class="card bcard">
                    <div class="card-header bgc-info-d1 text-white border-0">
                        <h4><span><i class="fas fa-users"></i></span> {{ trans('cruds.user.title_singular') }} {{ trans('global.list') }}</h4>
                        <div class="mb-2 mb-sm-0">
                            <a href="{{ route('admin.users.create') }}" class="btn btn-blue px-3 d-block w-100 text-95 radius-round border-2 brc-black-tp10">
                                <i class="fa fa-plus mr-1"></i>
                                {{ trans('global.add') }} <span class="d-sm-none d-md-inline">{{ trans('global.new') }}</span> {{ trans('cruds.user.title_singular') }}
                            </a>
                        </div>
                    </div>
                    <div class="card-body p-0 border-x-1 border-b-1 brc-default-m4 radius-0 overflow-hidden">
                        <div class="bootstrap-table bootstrap4">
                            <div class="fixed-table-toolbar">
                                <div class="columns columns-left float-left search btn-group">
                                    <span class="d-inline-block text-grey-d2 mt-2">
                                        Per page:
                                    </span>
                                    <select wire:model="perPage" class="ml-3 ace-select no-border angle-down brc-h-blue-m3 w-auto pr-45 text-secondary-d3">
                                            @foreach($paginationOptions as $value)
                                                <option value="{{ $value }}">{{ $value }}</option>
                                            @endforeach
                                    </select>
                                </div>

                                <div class="bs-bars float-left">
                                    <div id="table-toolbar">
                                    <button wire:click="$emit('deleteAll')" id="remove-btn" class="btn btn-xs p-2 mr-2 bgc-white btn-lighter-red radius-3px"
                                        @if ($selectPage || $selectAll)
                                            null
                                        @else
                                            disabled
                                        @endif
                                    >
                                    <i class="fa fa-trash-alt text-125 mx-2px"></i> {{ trans('global.bulk_delete') }}({{ count($selected)}})
                                    </button>
                                    </div>
                                </div>

                                @if ($selectPage)
                                    <div class="columns columns-left float-left text-nowrap align-self-center align-self-sm-start mt-3 ml-5 ">
                                        @if ($selectAll)
                                            <div>
                                                You have select All <strong>{{ $users->total() }}</strong> items.
                                            </div>
                                        @else
                                            <div>
                                                You have select <strong>{{ count($selected) }}</strong> items, Do you want to Select All
                                                <strong>{{ $users->total() }}</strong>?
                                                <a href="javascript:void(0)" wire:click="selectAll" class="ml-2">Select All</a>
                                            </div>
                                        @endif
                                    </div>
                                @endif

                                <div class="columns columns-right btn-group float-right">
                                        <button class="btn btn-outline-default bgc-white btn-h-light-primary btn-a-outline-primary py-1 px-25 text-95" type="button" name="fullscreen" aria-label="Fullscreen" title="Fullscreen"><i class="fa fa fa-expand"></i>
                                        </button>

                                        <div class="keep-open btn-group" title="Columns">
                                                <button class="btn btn-outline-default bgc-white btn-h-light-primary btn-a-outline-primary py-1 px-25 text-95 dropdown-toggle" type="button" data-toggle="dropdown" aria-label="Columns" title="Columns">
                                                        <i class="fa fa-th-list text-orange-d1"></i>
                                                        <span class="caret"></span>
                                                </button>

                                                <div class="dropdown-menu dropdown-menu-right">
                                                        <label class="dropdown-item dropdown-item-marker">
                                                                <input type="checkbox" data-field="id" value="1" checked="checked"> <span>Product ID</span>
                                                        </label>
                                                        <label class="dropdown-item dropdown-item-marker">
                                                                <input type="checkbox" data-field="name" value="2" checked="checked"> <span>Product Name</span>
                                                        </label><label class="dropdown-item dropdown-item-marker">
                                                                <input type="checkbox" data-field="price" value="3" checked="checked"> <span>Price</span>
                                                        </label>
                                                        <label class="dropdown-item dropdown-item-marker">
                                                                <input type="checkbox" data-field="tools" value="4" checked="checked"> <span><i class="fa fa-cog text-secondary-d1 text-130"></i></span>
                                                        </label>
                                                </div>
                                        </div>

                                        {{-- <button class="btn btn-outline-default bgc-white btn-h-light-primary btn-a-outline-primary py-1 px-25 text-95" type="button" name="print" aria-label="Print" title="Print"><i class="fa fa-print text-purple-d1"></i> </button> --}}

                                        <div class="export btn-group">
																						<button class="btn btn-outline-default bgc-white btn-h-light-primary btn-a-outline-primary py-1 px-25 text-95 dropdown-toggle" aria-label="Export" data-toggle="dropdown" type="button" title="Export data"><i class="fa fa-download text-blue"></i>
																										<span class="caret"></span>
																						</button>
																						<div class="dropdown-menu dropdown-menu-right">
																							@if(file_exists(app_path('Http/Livewire/ExcelExport.php')))
																									<livewire:excel-export model="User" format="csv" />
																									<livewire:excel-export model="User" format="xlsx" />
																									<livewire:excel-export model="User" format="pdf" />
																							@endif
																						</div>
                                        </div>
                                </div>

                                <div class="columns columns-right float-right search btn-group">
                                    <input wire:model.debounce.300ms="search" class="form-control search-input" type="search" placeholder="Search" autocomplete="off">
                                </div>
                            </div>

                            <div class="fixed-table-container" style="padding-bottom: 0px;">
                                <div class="fixed-table-body">
                                    <table class="table text-dark-m2 text-95 bgc-white ml-n1px table-bordered table-hover">
                                        <thead class="bgc-white text-grey text-uppercase text-80">
                                            <tr class="bgc-info text-white brc-black-tp10">
                                                <th class="bs-checkbox " style="width: 6%">
                                                    <div class="th-inner ">
                                                        <label>
                                                            <input wire:model="selectPage" name="btSelectAll" type="checkbox"><span></span>
                                                        </label>
                                                    </div>
                                                    <div class="fht-cell"></div>
                                                </th>
                                                <th style="width: 6%">
                                                        <div class="th-inner sortable both">
                                                            {{ trans('cruds.user.fields.id') }}
                                                            @include('components.table.sort', ['field' => 'id'])
                                                        </div>
                                                        <div class="fht-cell"></div>
                                                </th>
                                                <th style="width: 25%;">
                                                        <div class="th-inner sortable both">
                                                            {{ trans('cruds.user.fields.name') }}
                                                            @include('components.table.sort', ['field' => 'name'])
                                                        </div>
                                                        <div class="fht-cell"></div>
                                                </th>
                                                <th style="width: 25%;">
                                                        <div class="th-inner sortable both">
                                                            {{ trans('cruds.user.fields.email') }}
                                                            @include('components.table.sort', ['field' => 'email'])
                                                        </div>
                                                        <div class="fht-cell"></div>
                                                </th>
                                                <th style="width: 25%';">
                                                    <div class="th-inner sortable both">
                                                        {{ trans('cruds.user.fields.roles') }}
                                                    </div>
                                                    <div class="fht-cell"></div>
                                                </th>
                                                <th style="text-align: center; width: 10%; ">
                                                        <div class="th-inner "><i class="fa fa-cog text-secondary-d1 text-130"></i></div>
                                                        <div class="fht-cell"></div>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @forelse($users as $user)
                                                <tr>
                                                        <td class="bs-checkbox ">
                                                                <label>
                                                                        <input wire:model="selected" id="selected" value="{{ $user->id }}" name="btSelectItem" type="checkbox">
                                                                        <span></span>
                                                                </label>
                                                        </td>
                                                        <td>{{ $user->id }}</td>
                                                        <td>{{ $user->name }}</td>
                                                        <td>{{ $user->email }}</td>
                                                        <td>
                                                            @foreach($user->roles as $key => $entry)
                                                                <span class="badge badge-info">{{ $entry->title }}</span>
                                                            @endforeach
                                                        </td>
                                                        <td style="text-align: center; ">
                                                                <div class="action-buttons">
                                                                    @can('user_show')
                                                                        <a class="text-blue mx-1" href="{{ route('admin.users.show',$user->id) }}"><i class="fa fa-search-plus text-105"></i></a>
                                                                    @endcan
                                                                    @can('user_edit')
                                                                        <a class="text-success mx-1" href="{{ route('admin.users.edit',$user->id) }}"><i class="fa fa-pencil-alt text-105"></i></a>
                                                                    @endcan
                                                                    @can('user_delete')
                                                                        <a wire:click="$emit('deleteObject', {{ $user->id }})" class="text-danger-m1 mx-1"><i class="fa fa-trash-alt text-105"></i></a>
                                                                    @endcan
                                                                </div>
                                                        </td>
                                                </tr>
                                            @empty

                                            @endforelse
                                        </tbody>
                                        </table>
                                    </div>
                                    <div class="fixed-table-footer"></div>
                            </div>

                            <div class="fixed-table-pagination">
                                    <div class="float-left pagination-detail">
                                    </div>

                                    <div class="float-right pagination">
                                        {{ $users->links() }}
                                    </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
        </div>
	</div>

</div>
