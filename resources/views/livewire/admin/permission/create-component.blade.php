<div>
	<div class="row">
		<div class="col-lg-4">
				<div class="page-header">
						<h4 class="page-title text-primary-d2">
								<a href="{{ route('admin.permissions.index') }}">{{ trans('cruds.permission.title') }}</a>
								<span class="page-info text-secondary-d2 text-80">
										<i class="fa fa-angle-double-right text-80"></i>
										{{ trans('cruds.permission.title_singular') }} {{ trans('global.create') }}
								</span>
						</h4>
				</div>
		</div>
	</div>
	<div class="row justify-content-center">
		<div class="col-12 col-sm-6 mt-3 mt-sm-0 cards-container">
				<div class="card dcard overflow-hidden">
						<div class="card-header text-white">
								<h5 class="card-title text-primary text-110 pt-1">
										<span><i class="fa fa-user text-105"></i></span>
										{{ trans('global.create') }} {{ trans('cruds.permission.title_singular') }}
								</h5>

								<div class="card-toolbar no-border">
										<a href="#" data-action="toggle" class="card-toolbar-btn btn btn-sm radius-1 btn-outline-default btn-h-outline-default px-15 mx-0 btn-tp">
												<i class="fa fa-chevron-up w-2 mx-1px"></i>
										</a>
								</div>
						</div><!-- /.card-header -->
						<form wire:submit.prevent="submit" class="pt-3">
								<div class="card-body bg-white p-0 collapse show" style="">
										<div class="p-3">
												<div class="form-group">
														<label class="form-control-label">{{ trans('cruds.permission.fields.group') }}: <span class="text-danger">*</span>
																@error('permission.group') <span class="text-danger text-sm">{{ $message }}</span> @enderror
														</label>
														<input wire:model.defer="permission.group" class="form-control" type="text" id="group" value="" placeholder="Enter {{ trans('cruds.permission.fields.group') }}">
												</div>
												<br>
												<div class="form-group {{ $errors->has('permission.title') ? 'invalid' : '' }}">
														<label class="form-label required" for="title">{{ trans('cruds.permission.fields.title') }} <span class="text-danger">*</span>
																														@error('permission.title') <span class="text-danger text-sm">{{ $message }}</span> @enderror
																												</label>
														<input wire:model.defer="permission.title" class="form-control" type="text" name="title" id="title" placeholder="Enter {{ trans('cruds.permission.fields.title') }}">
												</div>
										</div>
								</div><!-- /.card-body -->
								<div class="card-footer bgc-white px-4 py-3 brc-dark-l3 d-flex justify-content-center">
										<a href="{{ route('admin.permissions.index') }}" class="btn border-2 btn-lighter-secondary btn-text-black btn-h-secondary btn-a-secondary btn-bgc-tp">
												<i class="fa fa-times text-110 text-danger-m1 mr-1"></i>
												{{ trans('global.cancel') }}
										</a>
										&nbsp;&nbsp;&nbsp;
										<button type="submit" class="btn border-2 btn-lighter-primary btn-text-black btn-h-primary btn-a-primary btn-bgc-tp">
												<i class="fa fa-check mr-1 text-success"></i>
												{{ trans('global.save') }}
										</button>
								</div><!-- /.card-footer -->
						</form>
				</div><!-- /.card -->
		</div>
	</div>
</div>
