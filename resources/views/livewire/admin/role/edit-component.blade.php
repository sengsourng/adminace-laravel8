<div>
	<div class="row">
			<div class="col-lg-4">
							<div class="page-header">
											<h5 class="page-title text-primary-d2">
													<a href="{{ route('admin.dashboard') }}"><i class="fa fa-home text-80"></i></a>
															<span class="page-info text-secondary-d2 text-80">
																	<i class="fa fa-angle-double-right text-80"></i>
																	<a href="{{ route('admin.roles.index') }}">{{ trans('cruds.role.title') }}</a>
															</span>
															<span class="page-info text-secondary-d2 text-80">
																	<i class="fa fa-angle-double-right text-80"></i>
																	{{ trans('global.edit') }} {{ trans('cruds.role.title_singular') }}
															</span>
											</h5>
							</div>
			</div>
	</div>

	<div class="row justify-content-center">
		<div class="col-lg-8">

			<div class="card border radius-0">
				<div class="card-header bgc-info-d1 text-white">
						<h5 class="card-title text-white">
								<i class="fa fa-table mr-2px"></i>
								{{ trans('global.edit') }} {{ trans('cruds.role.title_singular') }}
						</h5>
						<div class="mb-2 mb-sm-0">
							<a href="{{ route('admin.roles.index') }}" class="btn btn-green px-3 d-block w-100 text-95 radius-round border-2 brc-black-tp10">
								<i class="fa fa-list mr-1"></i>
								{{ trans('global.list') }} <span class="d-sm-none d-md-inline">{{ trans('cruds.role.title') }}</span>
							</a>
						</div>
				</div>

				<form wire:submit.prevent="submit" class="pt-3" action="#" id="frmObject" method="POST" enctype="multipart/form-data">
						@csrf
						<div class="card-body bg-white p-0 collapse show" style="">
								<div class="p-3">
										<div class="form-group {{ $errors->has('role.title') ? 'invalid' : '' }}">
												<label class="form-label required" for="title">{{ trans('cruds.role.fields.title') }} <span class="text-danger">*</span>
													@error('role.title') <span class="text-danger text-sm">{{ $message }}</span> @enderror
												</label>
												<input wire:model.defer="role.title" class="form-control" type="text" placeholder="Enter Title" id="title">
										</div>
										@include('livewire.admin.role.permission_form')
								</div>
						</div><!-- /.card-body -->
						<div class="card-footer bgc-white px-4 py-3 brc-dark-l3 d-flex justify-content-center">
								<a href="{{ route('admin.roles.index') }}" class="btn border-2 btn-lighter-secondary btn-text-black btn-h-secondary btn-a-secondary btn-bgc-tp">
									<i class="fa fa-times text-110 text-danger-m1 mr-1"></i>
									{{ trans('global.cancel') }}
								</a>
								&nbsp;&nbsp;&nbsp;
								<button type="submit" class="btn border-2 btn-lighter-primary btn-text-black btn-h-primary btn-a-primary btn-bgc-tp">
									<i class="fa fa-check mr-1 text-success"></i>
									{{ trans('global.save') }}
								</button>
						</div><!-- /.card-footer -->
				</form>
			</div> <!--card-->

		</div>
	</div>
</div>
