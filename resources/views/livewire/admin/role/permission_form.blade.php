<div class="form-group">
	<label class="form-control-label">{{ trans('cruds.role.fields.permissions') }}<span class="text-danger">*</span>
        @error('permissions') <span class="text-danger text-sm">{{ $message }}</span> @enderror
    </label>
    <br>
	<div class="col-sm-10">
			<table id="dynamic-table" class="table table-bordered table-hover">
				<thead class="table-secondary">
				<tr>
					<th width="20%">Group</th>
					<th class="center">
						<label class="pos-rel">
							<input wire:model="selectAll" type="checkbox" class="ace" />
							<span class="lbl"></span>
						</label>
					</th>
					<th>Access-{{ count($permissions) }}</th>
				</tr>
				</thead>
				<tbody>
				@if($all_permissions)
					@foreach($all_permissions as $permission)
						<tr>
							<td><strong>{{$permission[0]['group']}}</strong></td>
							<td class="center first-child">
							<label>
								<input wire:model="groups" value="{{ $permission[0]['group'] }}" type="checkbox" name="chkIds" class="ace group"/>
								<span class="lbl"></span>
							</label>
							</td>
							<td>
								@foreach($permission as $access)
									<label>
										<input wire:model="permissions" class="ace" type="checkbox" value="{{ $access['id'] }}">
										<span class="lbl pr-2">&nbsp;{{ $access['title']}} &nbsp;&nbsp;</span>
									</label>
								@endforeach
							</td>
						</tr>
					@endforeach
				@else
					<tr><td colspan="7">No data found.</td></tr>
				@endif
				</tbody>
			</table>
		</div>
</div>
