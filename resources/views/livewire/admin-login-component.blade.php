<div>
    <div class="body-container">
        <div class="main-container container bgc-transparent">
        <div class="main-content minh-80 justify-content-center">
            <div class="p-2 p-md-4">
                <div class="row" id="row-1">
                    <div class="col-12 col-xl-10 offset-xl-1 bgc-white shadow radius-1 overflow-hidden">
                        <div class="row" id="row-2">
                            <div id="id-col-intro" class="col-lg-5 d-none d-lg-flex border-r-1 brc-default-l3 px-0">
                                <!-- the left side section is carousel in this demo, to show some example variations -->

                                <div id="loginBgCarousel" class="carousel slide minw-100 h-100">
                                    <ol class="d-none carousel-indicators">
                                    <li data-target="#loginBgCarousel" data-slide-to="0" class="active"></li>
                                    <li data-target="#loginBgCarousel" data-slide-to="1"></li>
                                    <li data-target="#loginBgCarousel" data-slide-to="2"></li>
                                    <li data-target="#loginBgCarousel" data-slide-to="3"></li>
                                    </ol>

                                    <div class="carousel-inner minw-100 h-100">
                                    <div class="carousel-item active minw-100 h-100">
                                        <!-- default carousel section that you see when you open login page -->
                                        <div style="background-image: url({{asset('backend/ace/assets/image/login-bg-1.svg')}});" class="px-3 bgc-blue-l4 d-flex flex-column align-items-center justify-content-center">
                                        <a class="mt-5 mb-2" href="{{route('home')}}">
                                            {{-- <i class="fa fa-leaf text-success-m2 fa-3x"></i> --}}
                                            <img height="90" src="{{asset('images/logo_sengly_96.png')}}" alt="">

                                        </a>

                                        <h2 class="text-primary-d1">
                                            ចូលប្រើ <span class="text-110 text-dark-l1">ហាងខ្មែរ</span>
                                            {{-- <img height="120" src="{{asset('image/logo_banner.png')}}" alt=""> --}}
                                            {{-- <img height="120" src="{{asset('image/logo-512.png')}}" alt=""> --}}
                                        </h2>

                                        <div class="mt-5 mx-4 text-dark-tp3">
                                            <span class="text-120">
                                        ចុះឈ្មោះ ជាមួយ​​ហាងយើងខ្ញុំ​,<br /> ដើម្បីទទួលបានទំនិញល្អៗ និង​ជឿជាក់!
                                    </span>
                                            <hr class="mb-1 brc-black-tp10" />
                                            <div>
                                            <a id="id-start-carousel" href="#" class="text-95 text-dark-l2 d-inline-block mt-3">
                                                <i class="far fa-image text-110 text-purple-m1 mr-1 w-2"></i>
                                                ប្តូររូបភាពខាងក្រោយ
                                            </a>
                                            <br />
                                            <a id="id-remove-carousel" href="#" class="text-md text-dark-l2 d-inline-block mt-3">
                                                <i class="far fa-trash-alt text-110 text-orange-d1 mr-1 w-2"></i>
                                                ដកផ្នែកនេះចេញ
                                            </a>
                                            <br />
                                            <a id="id-fullscreen" href="#" class="text-md text-dark-l2 d-inline-block mt-3">
                                                <i class="fa fa-expand text-110 text-green-m1 mr-1 w-2"></i>
                                                ធ្វើអោយពេញអេក្រង់
                                            </a>
                                            </div>
                                        </div>

                                        <div class="mt-auto mb-4 text-dark-tp2">
                                            HangKhmer Co.Ltd &copy; 2021
                                        </div>
                                        </div>
                                    </div>

                                    <div class="carousel-item minw-100 h-100">
                                        <!-- the second carousel item with dark background -->
                                        <div style="background-image: url({{asset('backend/ace/assets/image/login-bg-2.svg')}});" class="d-flex flex-column align-items-center justify-content-start">
                                        <a class="mt-5 mb-2" href="{{route('home')}}">
                                            {{-- <i class="fa fa-leaf text-success-m2 fa-3x"></i> --}}
                                            <img height="90" src="{{asset('images/logo_sengly_96.png')}}" alt="">

                                        </a>

                                        <h2 class="text-blue-l1">
                                            ហាង <span class="text-110 text-white-tp3">ខ្មែរ</span>
                                        </h2>
                                        </div>
                                    </div>



                                    <div class="carousel-item minw-100 h-100">
                                        <div style="background-image: url({{asset('backend/backend/ace/assets/image/login-bg-3.jpg')}});" class="d-flex flex-column align-items-center justify-content-start">
                                        <div class="bgc-black-tp4 radius-1 p-3 w-90 text-center my-3 h-100">
                                            <a class="mt-5 mb-2" href="{{route('home')}}">
                                                {{-- <i class="fa fa-leaf text-success-m2 fa-3x"></i> --}}
                                                <img height="90" src="{{asset('images/logo_sengly_96.png')}}" alt="">

                                            </a>

                                            <h2 class="text-blue-l1">
                                            ហាង <span class="text-110 text-white-tp3">ខ្មែរ</span>
                                            </h2>
                                        </div>
                                        </div>
                                    </div>

                                    <div class="carousel-item minw-100 h-100">
                                        <div style="background-image: url({{asset('backend/ace/assets/image/login-bg-4.jpg)')}};" class="d-flex flex-column align-items-center justify-content-start">
                                            <a class="mt-5 mb-2" href="{{route('home')}}">
                                                {{-- <i class="fa fa-leaf text-success-m2 fa-3x"></i> --}}
                                                <img height="90" src="{{asset('images/logo_sengly_96.png')}}" alt="">

                                            </a>

                                        <h2 class="text-blue-d1">
                                            ហាង <span class="text-110 text-dark-tp3">ខ្មែរ</span>
                                        </h2>
                                        </div>
                                    </div>

                                    </div>
                                </div>
                            </div>

                            <div id="id-col-main" class="col-12 col-lg-7 py-lg-5 bgc-white px-0">
                            <!-- you can also use these tab links -->
                            <ul class="d-none mt-n4 mb-4 nav nav-tabs nav-tabs-simple justify-content-end bgc-black-tp11" role="tablist">
                                <li class="nav-item mx-2">
                                <a class="nav-link active px-2" data-toggle="tab" href="#id-tab-login" role="tab" aria-controls="id-tab-login" aria-selected="true">
                                    Login
                                </a>
                                </li>
                                <li class="nav-item mx-2">
                                <a class="nav-link px-2" data-toggle="tab" href="#id-tab-signup" role="tab" aria-controls="id-tab-signup" aria-selected="false">
                                    Signup
                                </a>
                                </li>
                            </ul>

                            <div class="tab-content tab-sliding border-0 p-0" data-swipe="right">
                                <div class="tab-pane active show mh-100 px-3 px-lg-0 pb-3" id="id-tab-login">
                                <!-- show this in desktop -->
                                <div class="d-none d-lg-block col-md-6 offset-md-3 mt-lg-4 px-0">
                                    <h4 class="text-dark-tp4 border-b-1 brc-secondary-l2 pb-1 text-130">
                                    <i class="fa fa-coffee text-orange-m1 mr-1"></i>
                                    ស្វា​គម​ន៏​ការ​ត្រ​លប់​មក​វិញ
                                    </h4>
                                </div>
                                <!-- show this in mobile device -->
                                <div class="d-lg-none text-secondary-m1 my-4 text-center">
                                    <a href="{{route('home')}}">
                                    <i class="fa fa-leaf text-success-m2 text-200 mb-4"></i>
                                    </a>
                                    <h1 class="text-170">
                                    <span class="text-blue-d1">
                                        កម្មវិធី <span class="text-80 text-dark-tp3">ហាងខ្មែរ</span>
                                    </span>
                                    </h1>
                                    ស្វា​គម​ន៏​ការ​ត្រ​លប់​មក​វិញ
                                </div>

                                @if (session('status'))
                                    <div class="mb-4 font-medium text-sm text-green-600">
                                        {{ session('status') }}
                                    </div>
                                @endif

                                {{-- Login --}}
                                <form autocomplete="off" method="POST" action="{{ route('login') }}" class="form-row mt-4" >
                                    @csrf
                                    <div class="form-group col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-lg-6 offset-lg-3">
                                    <div class="d-flex align-items-center input-floating-label text-blue brc-blue-m2">
                                        <input placeholder="អ៊ីម៉ែល" type="text" name="email" :value="old('email')" required autofocus class="form-control form-control-lg pr-4 shadow-none" id="email" />
                                        <i class="fa fa-user text-grey-m2 ml-n4"></i>
                                        <label class="floating-label text-grey-l1 ml-n3" for="email">
                                        អ៊ីម៉ែល
                                        </label>
                                    </div>
                                    </div>


                                    <div class="form-group col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-lg-6 offset-lg-3 mt-2 mt-md-1">
                                    <div class="d-flex align-items-center input-floating-label text-blue brc-blue-m2">
                                        <input placeholder="លេខសំងាត់​" type="password"
                                        name="password" required autocomplete="current-password"
                                        class="form-control form-control-lg pr-4 shadow-none" id="id-login-password" />

                                        <i class="fa fa-key text-grey-m2 ml-n4"></i>
                                        <label class="floating-label text-grey-l1 ml-n3" for="id-login-password">
                                            លេខសំងាត់​
                                        </label>
                                    </div>
                                    </div>


                                    <div class="col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-lg-6 offset-lg-3 text-right text-md-right mt-n2 mb-2">
                                        @if (Route::has('password.request'))
                                            <a href="{{ route('password.request') }}" class="text-primary-m1 text-95" data-toggle="tab" data-target="#id-tab-forgot">
                                                ភ្លេច​លេខសំងាត់​?
                                            </a>
                                        @endif
                                    </div>


                                    <div class="form-group col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-lg-6 offset-lg-3">
                                    <label class="d-inline-block mt-3 mb-0 text-dark-l1">
                                        {{-- <input type="checkbox" class="mr-1" id="id-remember-me" id="remember_me" name="remember" /> --}}
                                        <input type="checkbox" class="mr-1" id="remember_me" name="remember" />
                                        ចងចាំខ្ញុំ
                                    </label>

                                    <button type="submit" class="btn btn-primary btn-block px-4 btn-bold mt-2 mb-4">
                                        ចូលប្រើ
                                    </button>
                                    </div>

                                    <hr class="brc-default-l2 w-100 mb-2" />
                                    <div class="mt-n4 bgc-white-tp2 px-3 py-1 text-secondary-d3 text-90">ឬចុះឈ្មោះប្រើប្រាស់ ជាមួយ</div>

                                        <div class="mt-2 mb-3">
                                            <button type="button" class="btn btn-primary border-2 radius-round btn-lg mx-1">
                                            <i class="fab fa-facebook-f text-110"></i>
                                            </button>

                                            <button type="button" class="btn btn-blue border-2 radius-round btn-lg px-25 mx-1">
                                            <i class="fab fa-github text-110"></i>
                                            </button>

                                            <button type="button" class="btn btn-danger border-2 radius-round btn-lg px-25 mx-1">
                                            <i class="fab fa-google text-110"></i>
                                            </button>
                                        </div>
                                </form>

                                </div>
                            </div><!-- .tab-content -->
                            </div>

                        </div><!-- /.row -->

                    </div><!-- /.col -->
                </div><!-- /.row -->

                <div class="d-lg-none my-3 text-white-tp1 text-center">
                    <i class="fa fa-leaf text-success-l3 mr-1 text-110"></i> Sourng Company &copy; 2021
                </div>
            </div>
        </div>

        </div>

    </div>
</div>
