@extends('layouts.ace_admin_layout')

@push('styles')
    {{-- <link rel="stylesheet" href="{{ asset('backend/ace') }}/node_modules/basictable/dist/css/basictable.css"> --}}
    <link rel="stylesheet" href="{{ asset('backend/ace') }}/node_modules/bootstrap-table/dist/bootstrap-table.css">
@endpush

@section('admin_content')

	@livewire('admin.permission.index-component')

@endsection

@push('scripts')
	<script src="{{ asset('backend/ace') }}/node_modules/tableexport.jquery.plugin/tableExport.min.js"></script>
	<script src="{{ asset('backend/ace') }}/node_modules/bootstrap-table/dist/bootstrap-table.min.js"></script>
	<script src="{{ asset('backend/ace') }}/node_modules/bootstrap-table/dist/extensions/export/bootstrap-table-export.js"></script>
	<script src="{{ asset('backend/ace') }}/node_modules/bootstrap-table/dist/extensions/print/bootstrap-table-print.js"></script>
	<script src="{{ asset('backend/ace') }}/node_modules/bootstrap-table/dist/extensions/mobile/bootstrap-table-mobile.js"></script>
  {{-- <script src="{{ asset('backend/ace') }}/node_modules/basictable/dist/js/jquery.basictable.js"></script>
	<script>
		jQuery(function($) {
			// highlight simple table row when selected
			function _highlight(row, checked) {
					// `classList.toggle` with 2 arguments isn't supported in IE10+
					// row.classList.toggle('active', checked)
					// row.classList.toggle('bgc-yellow-l3', checked)
					// row.classList.toggle('bgc-h-default-l3', !checked)

					if (checked) {
							row.classList.add('active')
							row.classList.add('bgc-success-l3')
							row.classList.remove('bgc-h-default-l3')
					}
					else {
							row.classList.remove('active')
							row.classList.remove('bgc-success-l3')
							row.classList.add('bgc-h-default-l3')
					}
			}

			$('#simple-table tbody tr')
			.on('click', function(e) {
					var ret = false
					try {
							// return if clicked on a .btn or .dropdown
							ret = e.target.classList.contains('btn') || e.target.parentNode.classList.contains('btn')|| e.target.closest('.dropdown') != null
					} catch(err) {}

					if (ret) return

					var inp = this.querySelector('input')
					if(inp == null) return

					if(e.target.tagName != "INPUT") {
							inp.checked = !inp.checked
					}
					_highlight(this, inp.checked)
			})

			$('#simple-table thead input')
			.on('change', function() {
					var checked = this.checked
					$('#simple-table tbody input[type=checkbox]')
					.each(function() {
							this.checked = checked
							var row = $(this).closest('tr').get(0)
							_highlight(row, checked)
					})
			})

			// responsive table using basic table plugin
			$('#responsive-table').basictable({breakpoint: 800})
			})
	</script> --}}

  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
	<script>
			Livewire.on('deleteObject',objectId =>{
				Swal.fire({
						title: 'Are you sure?',
						text: "You won't be able to revert this!",
						icon: 'warning',
						showCancelButton: true,
						confirmButtonColor: '#3085d6',
						cancelButtonColor: '#d33',
						confirmButtonText: 'Yes, delete it!'
				}).then((result) => {
						if (result.value) {
							Livewire.emitTo('admin.permission.index-component','delete',objectId);
							Swal.fire({
								position: 'top-end',
								icon: 'success',
								title: 'Your Data has been deleted',
								showConfirmButton: false,
								timer: 1500
							})
							// toastr.success('YYour Data has been deleted');
						}
				})
			});

			Livewire.on('deleteAll',checkAll => {
				Swal.fire({
						title: 'Are you sure?',
						text: "You won't be able to revert this!",
						icon: 'warning',
						showCancelButton: true,
						confirmButtonColor: '#3085d6',
						cancelButtonColor: '#d33',
						confirmButtonText: 'Yes, delete it!'
				}).then((result) => {
						if (result.value) {
							Livewire.emitTo('admin.permission.index-component','deleteSelected');
							// toastr.success('YYour Data has been deleted');
							Swal.fire({
								position: 'top-end',
								icon: 'success',
								title: 'Your Data has been deleted',
								showConfirmButton: false,
								timer: 1500
							})
						}
					})
			});
	</script>
@endpush
