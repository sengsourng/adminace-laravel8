@extends('layouts.ace_admin_layout')

@section('admin_content')

    @livewire('admin.permission.edit-component', [$permission])

@endsection
