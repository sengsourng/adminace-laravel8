@extends('layouts.ace_admin_layout')

@section('admin_content')

	<div class="row">
					<div class="col-lg-4">
							<div class="page-header">
									<h4 class="page-title text-primary-d2">
											<a href="{{ route('admin.users.index') }}">{{ trans('cruds.user.title') }}</a>
											<span class="page-info text-secondary-d2 text-80">
													<i class="fa fa-angle-double-right text-80"></i>
													{{ trans('cruds.user.title_singular') }} {{ trans('global.view') }}
											</span>
									</h4>
							</div>
					</div>
	</div>

	<div class="row">
		<div class="col-lg-4">
			<div class="card bg-blueGray-100">
				<div class="card-header">
					<div class="card-header-container">
						<h6 class="card-title">
							{{ trans('global.view') }}
							{{ trans('cruds.user.title_singular') }}:
							{{ trans('cruds.user.fields.id') }}
							{{ $user->id }}
						</h6>
					</div>
				</div>

				<div class="card-body">
						<div class="pt-3">
								<table class="table table-view table-bordered table-striped">
										<tbody class="bg-white">
												<tr>
														<th>
																{{ trans('cruds.user.fields.id') }}
														</th>
														<td>
																{{ $user->id }}
														</td>
												</tr>
												<tr>
														<th>
																{{ trans('cruds.user.fields.name') }}
														</th>
														<td>
																{{ $user->name }}
														</td>
												</tr>
												<tr>
														<th>
																{{ trans('cruds.user.fields.email') }}
														</th>
														<td>
																<a class="link-light-blue" href="mailto:{{ $user->email }}">
																		<i class="far fa-envelope fa-fw">
																		</i>
																		{{ $user->email }}
																</a>
														</td>
												</tr>
												<tr>
														<th>
																{{ trans('cruds.user.fields.email_verified_at') }}
														</th>
														<td>
																{{ $user->email_verified_at }}
														</td>
												</tr>
												<tr>
														<th>
																{{ trans('cruds.user.fields.roles') }}
														</th>
														<td>
																@foreach($user->roles as $key => $entry)
																		<span class="badge badge-relationship">{{ $entry->title }}</span>
																@endforeach
														</td>
												</tr>
										</tbody>
								</table>
						</div>
						<div class="form-group">
								@can('user_edit')
									<a href="{{ route('admin.users.edit', $user) }}" class="btn btn-sm btn-success mr-2">
										<i class="fa fa-edit text-110 text-white-m1 mr-1"></i>{{ trans('global.edit') }}
									</a>
								@endcan
								<a href="{{ route('admin.users.index') }}" class="btn btn-sm btn-secondary">
									<i class="fa fa-times text-110 text-danger-m1 mr-1"></i> {{ trans('global.back') }}
								</a>
						</div>
				</div>
			</div>
		</div>
	</div>

@endsection
