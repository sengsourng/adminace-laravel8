@extends('layouts.ace_admin_layout')

@section('admin_content')

    @livewire('admin.role.edit-component', [$role])

@endsection
