<?php

namespace App\Http\Controllers;

use Exception;
use App\Models\Team;

use App\Models\Admin\User;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;
use Laravel\Socialite\Facades\Socialite;



class SocialLoginController extends Controller
{
	public function redirectToProvider($provider)
	{
		return Socialite::driver($provider)->redirect();
	}

	public function handleProviderCallback($provider)
	{
		try {
			//create a user using socialite driver google
			$user = Socialite::driver($provider)->stateless()->user();
				if($provider=='github'){
					$avatar = $user->avatar;
				} elseif($provider=='google'){
					$avatar = explode('=',$user->avatar);
					$avatar = $avatar[0];
				}
				$url = $avatar;
				// save iamge from url to public folder
				// $img_name = public_path('images') .'\\'. Str::slug($user->name).'-'.rand().'-'.time().'.jpg';
				// file_put_contents($img_name, file_get_contents($url));
				if(!Storage::disk('public')->exists('profile-photos'))
				{
					Storage::disk('public')->makeDirectory('profile-photos');
				}
				$imageName = "user-".Carbon::now()->timestamp .'.jpg';
				Storage::disk('public')->put('profile-photos/'.$imageName,file_get_contents($url) );
				$avatar=$imageName;
				// if the user exits, use that user and login
				$finduser = User::where('social_id', $user->id)->first();
				if($finduser){
					//if the user exists, login and show dashboard
					Auth::login($finduser);
					return redirect('/dashboard');
				}else{
				//user is not yet created, so create first
				$useremail = $user->email;
				$unames = explode("@", $useremail);
				$uname = $unames[0]??null;
				$newUser = User::create([
					'name' => $user->name,
          'username' => $uname,
					'email' => $user->email,
					'social_id'=> $user->id,
					'avatar'=>$avatar,
					'profile_photo_path'=> 'profile-photos/'.$avatar,
					'password' => Hash::make('123456')
				]);
				// assign role for social user
        $newUser->roles()->sync(env('ROLE_FOR_USER'));
				//every user needs a team for dashboard/jetstream to work.
				//create a personal team for the user
				$newTeam = Team::forceCreate([
					'user_id' => $newUser->id,
					'name' => explode(' ', $user->name, 2)[0]."'s Team",
					'personal_team' => true,
				]);
				// save the team and add the team to the user.
				$newTeam->save();
				$newUser->current_team_id = $newTeam->id;
				$newUser->save();
				//login as the new user
				Auth::login($newUser);
				// go to the dashboard
				return redirect('/dashboard');
			}
			//catch exceptions
		} catch (Exception $e) {
			dd($e->getMessage());
		}
	}
}
