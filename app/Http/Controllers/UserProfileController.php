<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserProfileController extends Controller
{
    public function show(Request $request)
    {
        $this->authorize('auth_profile_edit');
        return view('profile.show');
    }
}
