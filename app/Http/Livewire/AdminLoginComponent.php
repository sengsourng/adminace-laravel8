<?php

namespace App\Http\Livewire;

use Livewire\Component;

class AdminLoginComponent extends Component
{
    public function render()
    {
        return view('livewire.admin-login-component')->layout('layouts.login');
    }
}
