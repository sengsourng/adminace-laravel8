<?php

namespace App\Http\Livewire\Admin\User;

use Livewire\Component;
use App\Models\Admin\Role;
use App\Models\Admin\User;

class CreateComponent extends Component
{
    public User $user;

    public array $roles = [];

    public string $password = '';

    public array $listsForFields = [];

		public $password_confirmation = '';

    public function mount(User $user)
    {
			$this->user = $user;
			$this->initListsForFields();
    }

    public function render()
    {
        return view('livewire.admin.user.create-component');
    }


    public function submit()
    {
			$this->validate();
			$this->user->password = $this->password;
			$this->user->save();
			$this->user->roles()->sync($this->roles);
			return redirect()->route('admin.users.index');
    }

    protected function rules(): array
    {
			return [
					'user.name' => [
							'string',
							'required',
              'max:50',
					],
					'user.email' => [
							'email:rfc',
							'required',
							'unique:users,email',
					],
					'password' => [
							'string',
							'required',
							'confirmed'
					],
                    'password_confirmation'=>[
                        'string',
                        'required'
                    ],
					'roles' => [
							'required',
							'array',
					],
					'roles.*.id' => [
							'integer',
							'exists:roles,id',
					],
			];
    }

    protected function initListsForFields(): void
    {
        $this->listsForFields['roles'] = Role::pluck('title', 'id')->toArray();
    }
}
