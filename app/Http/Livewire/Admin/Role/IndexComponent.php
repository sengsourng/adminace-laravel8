<?php

namespace App\Http\Livewire\Admin\Role;

use Livewire\Component;
use App\Models\Admin\Role;
use Livewire\WithPagination;
use Illuminate\Http\Response;
use App\Http\Livewire\WithSorting;
use Illuminate\Support\Facades\Gate;
use App\Http\Livewire\WithConfirmation;

class IndexComponent extends Component
{
	use WithPagination;
	use WithSorting;
	use WithConfirmation;

	public int $perPage;

	public array $orderable;

	public string $search = '';

	public array $selected = [];

	public array $paginationOptions;

	public $selectPage = false;

	public $selectAll = false;

  protected $paginationTheme = 'bootstrap';
  
	protected $listeners = ['delete','deleteSelected'=>'deleteSelected'];

	protected $queryString = [
			'search' => [
					'except' => '',
			],
			'sortBy' => [
					'except' => 'id',
			],
			'sortDirection' => [
					'except' => 'desc',
			],
	];

	public function updatedSelectPage($value)
	{
			if($value){
				$this->selected = $this->roles->paginate($this->perPage)->pluck('id')->map(fn ($item) => (string) $item)->toArray();
			} else {
				$this->selected = [];
			}
	}

	public function updatedSelectAll($value)
	{
		if($value){
			$this->selectAll = true;
		} else {
			$this->selectAll = false;
		}
	}

	public function selectAll()
	{
		$this->selectAll = true;
        $this->selected = $this->roles->pluck('id')->map(fn ($item) => (string) $item)->toArray();
	}

	public function updatedSelected($value)
	{
		if($value){
				$this->selectPage = false;
				$this->selectAll= true;
		} else {
				$this->selectAll=false;
		}
	}

	public function getRolesProperty()
	{
		return Role::with(['permissions'])->advancedFilter([
					's'               => $this->search ?: null,
					'order_column'    => $this->sortBy,
					'order_direction' => $this->sortDirection,
				]);
	}

	public function render()
	{
			$query = $this->roles;
			$roles = $query->paginate($this->perPage);
			return view('livewire.admin.role.index-component', compact('query', 'roles', 'roles'));
	}

	public function getSelectedCountProperty()
	{
			return count($this->selected);
	}

	public function updatingSearch()
	{
			$this->resetPage();
	}

	public function updatingPerPage()
	{
			$this->resetPage();
	}

	public function resetSelected()
	{
			$this->selected = [];
	}

	public function mount()
	{
			$this->sortBy            = 'id';
			$this->sortDirection     = 'asc';
			$this->perPage           = 10;
			$this->paginationOptions = config('project.pagination.options');
			$this->orderable         = (new Role())->orderable;
	}

	public function deleteSelected()
	{
		abort_if(Gate::denies('role_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');
		Role::whereIn('id', $this->selected)->delete();
		$this->resetSelected();
	}

	public function delete(Role $role)
	{
			abort_if(Gate::denies('role_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');
			$role->delete();
	}

}
