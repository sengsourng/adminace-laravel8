<?php

namespace App\Http\Livewire\Admin\Role;

use Livewire\Component;
use App\Models\Admin\Role;
use App\Models\Admin\Permission;

class EditComponent extends Component
{
	public Role $role;

	public array $permissions = [];

	public array $all_permissions = [];

	public $selectAll = false;

	public $groups = [];

	public function mount(Role $role)
	{
			$this->role        = $role;
			$this->permissions = $this->role->permissions()->pluck('id')->map(fn ($item)=>(string) $item)->toArray();
			$this->initListsForFields();
	}

	public function render()
	{
			return view('livewire.admin.role.edit-component');
	}

	public function submit()
	{
			// dd($this->validate());
			$this->validate();

			$this->role->save();
			$this->role->permissions()->sync($this->permissions);

			return redirect()->route('admin.roles.index');
	}

	protected function rules(): array
	{
		return [
				'role.title' => [
						'string',
						'required',
				],
				'permissions' => [
						'required',
						'array',
				],
				'permissions.*.id' => [
						'integer',
						'exists:permissions,id',
				],
		];
	}

	public function updatedGroups($groups)
	{
		if($groups){
			$groupcheck =  Permission::whereIn('group',$groups)->pluck('id')->map(fn ($item)=>(string) $item)->toArray();
			$this->permissions = $groupcheck;
			$this->selectAll=false;
		} else {
			$this->permissions =[];
		}
	}

	public function updatedPermissions($value)
	{
		if($value){
				$this->selectAll=false;
				$this->groups = [];
		} else {
				$this->groups = [];
		}
	}

	public function updatedSelectAll($value)
	{
		if($value){
			$this->permissions = Permission::pluck('id')->map(fn ($item)=>(string) $item)->toArray();
			$this->groups = [];
		} else {
			$this->permissions = [];
			$this->groups = [];
		}
	}

	public function initListsForFields()
	{
		$this->all_permissions = Permission::all()->groupBy('group')->toArray();
	}
}
