<?php

namespace App\Http\Livewire\Admin\Permission;

use Livewire\Component;
use Livewire\WithPagination;
use Illuminate\Http\Response;
use App\Models\Admin\Permission;
use App\Http\Livewire\WithSorting;
use Illuminate\Support\Facades\Gate;
use App\Http\Livewire\WithConfirmation;

class IndexComponent extends Component
{
	use WithPagination;
	use WithSorting;
	use WithConfirmation;

  protected $paginationTheme = 'bootstrap';

	public int $perPage;

	public array $orderable;

	public string $search = '';

	public array $selected = [];

	public array $paginationOptions;

	public $selectPage = false;

	public $selectAll = false;

	protected $listeners = ['delete','deleteSelected'=>'deleteSelected'];

	protected $queryString = [
			'search' => [
					'except' => '',
			],
			'sortBy' => [
					'except' => 'id',
			],
			'sortDirection' => [
					'except' => 'desc',
			],
	];

  public function updatedSelectPage($value)
	{
		if($value){
			$this->selected = $this->permissions->paginate($this->perPage)->pluck('id')->map(fn ($item) => (string) $item)->toArray();
		} else {
			$this->selected = [];
		}
	}

	public function updatedSelectAll($value)
	{
		if($value){
			$this->selectAll = true;
		} else {
			$this->selectAll = false;
		}
	}

	public function selectAll()
	{
		$this->selectAll = true;
		$this->selected = $this->permissions->pluck('id')->map(fn ($item) => (string) $item)->toArray();
	}

	public function updatedSelected($value)
	{
		if($value){
				$this->selectPage = false;
				$this->selectAll= true;
		} else {
				$this->selectAll=false;
		}
	}

	public function getPermissionsProperty()
	{
		return Permission::advancedFilter([
            's'               => $this->search ?: null,
            'order_column'    => $this->sortBy,
            'order_direction' => $this->sortDirection,
          ]);
	}

    public function render()
	{
		$query = $this->permissions;
		$permissions = $query->paginate($this->perPage);
		return view('livewire.admin.permission.index-component', compact('query', 'permissions', 'permissions'));
	}

	public function getSelectedCountProperty()
	{
		return count($this->selected);
	}

	public function updatingSearch()
	{
		$this->resetPage();
	}

	public function updatingPerPage()
	{
		$this->resetPage();
	}

	public function resetSelected()
	{
		$this->selected = [];
	}

	public function mount()
	{
		$this->sortBy            = 'id';
		$this->sortDirection     = 'asc';
		$this->perPage           = 10;
		$this->paginationOptions = config('project.pagination.options');
		$this->orderable         = (new Permission())->orderable;
	}

	public function deleteSelected()
	{
		abort_if(Gate::denies('permission_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');
		Permission::whereIn('id', $this->selected)->delete();
		$this->resetSelected();
	}

	public function delete(Permission $permission)
	{
		abort_if(Gate::denies('permission_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');
		$permission->delete();
	}
}
