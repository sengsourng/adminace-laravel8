<?php

namespace App\Http\Livewire\Admin\Permission;

use Livewire\Component;
use App\Models\Admin\Permission;

class CreateComponent extends Component
{
	public function render()
	{
		return view('livewire.admin.permission.create-component');
	}

	public Permission $permission;

	public function mount(Permission $permission)
	{
			$this->permission = $permission;
	}

	public function submit()
	{
			$this->validate();

			$this->permission->save();

			return redirect()->route('admin.permissions.index');
	}

	protected function rules(): array
	{
			return [
					'permission.title' => [
							'string',
							'required',
					],
					'permission.group' => [
							'string',
							'required',
					],
			];
	}
}
