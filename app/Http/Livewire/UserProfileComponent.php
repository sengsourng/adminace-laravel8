<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Province;
use App\Models\Admin\User;
use Livewire\WithFileUploads;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Intervention\Image\Facades\Image as ImageResize;
use Illuminate\Support\Facades\Storage;

class UserProfileComponent extends Component
{
    use WithFileUploads;

    public $users;
    public $user;
    public $user_id;
    public $name;
    public $email;
    public $username;
    public $phone;
    public $role_id;
    public $user_type;
    public $profile_photo_path;
		public $avatar;
    public $newimage ;
    public $updated_at;
    public $created_at;

    public $current_password;
    public $password;
    public $password_confirmation;

    public $address;

    public $province_id;
    public $current_team_id;

    public function mount()
    {
        $user=User::findOrFail(Auth::user()->id);
        $this->user_id=$user->id;
        $this->name=$user->name;
        $this->email=$user->email;
        $this->username=$user->username;
        $this->phone=$user->phone;
        $this->address=$user->address;
        $this->province_id=$user->province_id;
        $this->user_type=$user->user_type;
        $this->role_id=$user->role_id;
        $this->profile_photo_path=$user->profile_photo_path;
        $this->avatar=$user->avatar;
    }

    public function updated($fields)
    {
			$this->validateOnly($fields,[
					'name'  => 'required',
					'email'  => 'required',
					// 'username'  => 'required',
			]);

			if($this->newimage){
					$this->validateOnly($fields,[
							'newimage' => 'required|mimes:png,jpg,jpeg'
					]);
			}
    }

    public function updateProfile()
    {
			$this->validate([
					'name'  => 'required',
					'address'  => 'required',
					// 'username'  => 'required|unique',
					// 'image'  => 'required|mimes:png,jpg,jpeg',
			]);

			if($this->newimage){
				$this->validate([
					'newimage' => 'required|mimes:png,jpg,jpeg'
				]);
			}
			$user = User::find($this->user_id);
			$user->name=$this->name;
			$user->username=$this->username;
			$user->email=$this->email;
			$user->phone=$this->phone;
			$user->address=$this->address;
			$user->province_id=$this->province_id;

			if($this->newimage){
				if(!Storage::disk('public')->exists('profile-photos'))
				{
					Storage::disk('public')->makeDirectory('profile-photos');
				}
				if(Storage::disk('public')->exists($user->profile_photo_path)){
					Storage::disk('public')->delete($user->profile_photo_path);
				}
				if($user->profile_photo_path==null){
					$imageName="user-".Carbon::now()->timestamp.'.'. $this->newimage->extension();
					$image_resize =ImageResize::make($this->newimage->getRealPath())->resize(config('project.resize.width'),config('project.resize.height'))->stream();
					Storage::disk('public')->put('profile-photos/'.$imageName,$image_resize);
					$user->profile_photo_path="profile-photos/".$imageName;
					$user->avatar=$imageName;
					$this->avatar=$imageName;
				}else{
					$imageName="user-".Carbon::now()->timestamp.'.'. $this->newimage->extension();
					$image_resize =ImageResize::make($this->newimage->getRealPath())->resize(config('project.resize.width'),config('project.resize.height'))->stream();
					Storage::disk('public')->put('profile-photos/'.$imageName,$image_resize);
					$user->profile_photo_path="profile-photos/".$imageName;
					$user->avatar=$imageName;
					$this->avatar=$imageName;
				}
			} else {
                $user->profile_photo_path=$user->profile_photo_path;
                $user->avatar=$user->avatar;
			}
			$user->save();
			session()->flash('message','user has been updated!');
    }

		public function updatePassword()
    {
			$this->validate([
					'current_password' => ['required', 'string'],
					'password' => ['required', 'string'],
			]);

			$user=User::find($this->user_id);
			// $user->name=$this->name;
			$user->password=Hash::make($this->password);
			// $user->username=$this->username;
			// $user->email=$this->email;
			// $user->phone=$this->phone;
			// $user->address=$this->address;
			// $user->province_id=$this->province_id;
			$user->save();
			session()->flash('message','Password has been updated!');
    }

		public function render()
    {
        $user=User::where('id',Auth::user()->id);
        $provinces=Province::get();

        return view('livewire.user-profile-component',
										['user'=>$user,'provinces'=>$provinces]
										)->layout('layouts.ace');
    }
}
