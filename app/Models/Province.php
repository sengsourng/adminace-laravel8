<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    use HasFactory;

    protected $table="provinces";
    protected $fillable=([
        'khmer_name','name','code','type'
    ]);


    // public function users() {
    //     return $this->belongsToMany(User::class,'user_id');
    //  }
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
