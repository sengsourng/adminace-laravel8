<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\RoleController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Livewire\Admin\DashboardComponent;
use App\Http\Controllers\Admin\PermissionController;


Route::get('/', function () {
    return view('welcome');
});

// Route::middleware(['auth:sanctum,web', 'verified'])->get('/dashboard', function () {
//     return view('dashboard');
// })->name('dashboard');

Route::group(['prefix'=>'admin','as'=>'admin.','middleware'=>['auth:sanctum,web','verified']],function(){
    Route::get('dashboard',DashboardComponent::class)->name('dashboard');

  // Permissions
	Route::resource('permissions', PermissionController::class, ['except' => ['store', 'update', 'destroy']]);

	// Roles
	Route::resource('roles', RoleController::class, ['except' => ['store', 'update', 'destroy']]);

	// Users
	Route::resource('users', UserController::class, ['except' => ['store', 'update', 'destroy']]);
});
