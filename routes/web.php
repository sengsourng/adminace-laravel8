<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;
use App\Http\Livewire\AdminLoginComponent;
use App\Http\Livewire\UserProfileComponent;
use App\Http\Controllers\SocialLoginController;
use App\Http\Controllers\UserProfileController;


Route::get('/', function () {
    return view('welcome');
});

Route::get('/home', function () {
    return view('dashboard');
})->name('home');


Route::get('lang/{locale}',function($locale){
    Session::put('locale',$locale);
    return redirect()->back();
});

// Route::middleware(['auth:sanctum,web', 'verified'])->get('/dashboard', function () {
//     return view('dashboard');
// })->name('dashboard');

Route::get('/secure',AdminLoginComponent::class)->name('admin.login');

Route::group(['prefix'=>'','as'=>'','middleware'=>['auth:sanctum,web','verified']],function(){
    Route::get('/dashboard',function(){
        return view('dashboard');
    })->name('dashboard');
});

Route::group(['prefix' => 'profile', 'as' => 'profile.', 'middleware' => ['auth:sanctum,web']], function () {
    if (file_exists(app_path('Http/Controllers/UserProfileController.php'))) {
        Route::get('/', [UserProfileController::class, 'show'])->name('show');
    }
});

Route::get('user/profile',UserProfileComponent::class)->name('setting.profile');

Route::get('/auth/{provider}',[SocialLoginController::class,'redirectToProvider']);
Route::get('/auth/{provider}/callback',[SocialLoginController::class,'handleProviderCallback']);
